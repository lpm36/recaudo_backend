package pe.com.integra.api.recaudo.domain.dao.model.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.integra.api.recaudo.domain.dao.model.Planilla;
import pe.com.integra.api.recaudo.infraestructure.util.Utils;

public class BusquedaPlanillaCabeceraMapping implements RowMapper<Planilla> {

	@Override
	public Planilla mapRow(ResultSet rs, int arg1) throws SQLException {

		Planilla obj = new Planilla();
		obj.setLote(rs.getBigDecimal("PLANU3"));
		obj.setPlanillaFisico(rs.getBigDecimal("PLANUM"));
		obj.setPlanillaInterno(rs.getBigDecimal("PLANU1"));
		obj.setNroPreimpreso(Utils.validString(rs.getString("PLAN01")));
		obj.setTipoPlanilla(Utils.validString(rs.getString("PLATIP")));
		obj.setEstadoPlanilla(Utils.validString(rs.getString("PLAEST")));
		obj.setEstadoConciliacion(Utils.validString(rs.getString("PLAES4")));
		obj.setTipoPagoPlanilla(Utils.validString(rs.getString("PLAT03")));
		obj.setPeriodoDevengue(Utils.validString(rs.getString("PLAPER")));
		obj.setFechaPago(Utils.validString(rs.getString("PLAFEC")));
		obj.setCodigoEntidadRec(Utils.validString(rs.getString("PLACO2")));
		obj.setTipoIdentificacion(Utils.validString(rs.getString("PLATI1")));
		obj.setNumeroIdentificacion(Utils.validString(rs.getString("PLANU4")));
		obj.setTotalAfiliados(rs.getBigDecimal("PLAT04"));
		obj.setTotalRemuneraciones(rs.getBigDecimal("PLATOT"));
		obj.setTotalCotizacionObligatorio(rs.getBigDecimal("PLAT05"));
		obj.setSubtotalFondoPensiones(rs.getBigDecimal("PLASUB"));
		obj.setPenalidadesFondo(rs.getBigDecimal("PLACOS"));
		obj.setTotalPagarFondo(rs.getBigDecimal("PLATO4"));
		obj.setSumaAportesInformados(rs.getBigDecimal("PLATO5"));
		obj.setSumaAportesFondoCalculados(rs.getBigDecimal("PLAV07"));
		obj.setValorConciliadoFondo(rs.getBigDecimal("PLAVAL"));
		obj.setNroCuotasConciliadas(rs.getBigDecimal("PLAN03"));
		obj.setValorPagarEfectivoFondo(rs.getBigDecimal("PLAVA4"));
		obj.setPlanillaAPagar(rs.getBigDecimal("PLAPLA"));
		obj.setIndicadorForzarAcreditacion(Utils.validString(rs.getString("PLAI06")));
		obj.setIndicadorDos(Utils.validString(rs.getString("PLAI07")));
		obj.setIndicadorTres(Utils.validString(rs.getString("PLAI08")));

		obj.setAfp(Utils.validString(rs.getString("AFP")));
		
		return obj;
	}
}