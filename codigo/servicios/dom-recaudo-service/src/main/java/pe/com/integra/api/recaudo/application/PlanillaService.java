package pe.com.integra.api.recaudo.application;

import java.math.BigDecimal;

import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDTO;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseObtenerCabeceraPlanillasDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseBusquedaPlanillasDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseBusquedaPlanillasDetalleDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseObtenerPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;

public interface PlanillaService {

	public ResponseBusquedaPlanillasDTO buscarPlanillas(RequestBusquedaPlanillaDTO request);

	public ResponseObtenerCabeceraPlanillasDTO obtenerCabeceraPlanilla(String usuario, BigDecimal lotePlanilla,
			BigDecimal numeroPlanilla);

	public ResponseBusquedaPlanillasDetalleDTO buscarPlanillasDetalles(RequestBusquedaPlanillaDetalleDTO request);

	public ResponseObtenerPlanillaDetalleDTO obtenerDetallePlanilla(PlanillaDetalle parametrosDetalle);

}
