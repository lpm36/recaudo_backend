package pe.com.integra.api.recaudo.infraestructure.util.annotation;

import java.util.Map;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDetalleDTO;

public class ValidadorBusquedaPlanillaDetalle
		implements ConstraintValidator<BusquedaPlanillaDetalleValida, RequestBusquedaPlanillaDetalleDTO> {

	@Override
	public void initialize(BusquedaPlanillaDetalleValida constraintAnnotation) {
		// Se sobreescribe el metodo por causar conflicto en Jboss
	}

	@Override
	public boolean isValid(RequestBusquedaPlanillaDetalleDTO request, ConstraintValidatorContext context) {
		StringBuilder mensaje = new StringBuilder();

		if (request.getLote() == null
				&& (request.getPeriodoDevengue() == null || request.getPeriodoDevengue().isEmpty())
				&& request.getPlanillaInterno() == null
				&& (request.getCuspp() == null || request.getCuspp().isEmpty())) {
			context.disableDefaultConstraintViolation();

			mensaje.append("Error1");

			context.buildConstraintViolationWithTemplate("Un criterio de primer nivel es necesario").addBeanNode()
					.addConstraintViolation();
		}

		if ((request.getTipoIdentificacion() != null && !request.getTipoIdentificacion().isEmpty())
				&& (request.getNumeroIdentificacion() == null || request.getNumeroIdentificacion().isEmpty())) {
			context.disableDefaultConstraintViolation();

			mensaje.append("Error2");

			context.buildConstraintViolationWithTemplate(
					"Si ingreso el tipo de identificacion debe ingresar el numero de identificacion").addBeanNode()
					.addConstraintViolation();
		}

		if ((request.getNumeroIdentificacion() != null && !request.getNumeroIdentificacion().isEmpty())
				&& (request.getTipoIdentificacion() == null || request.getTipoIdentificacion().isEmpty())) {
			context.disableDefaultConstraintViolation();

			mensaje.append("Error3");

			context.buildConstraintViolationWithTemplate(
					"Si ingreso el numero de identificacion debe ingresar el tipo de identificacion").addBeanNode()
					.inContainer(Map.class, 1).addConstraintViolation();
		}

		if (!mensaje.toString().isEmpty()) {
			return false;
		}

		return true;
	}
}