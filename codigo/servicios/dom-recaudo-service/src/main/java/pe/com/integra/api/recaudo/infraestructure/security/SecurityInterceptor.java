package pe.com.integra.api.recaudo.infraestructure.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.integra.api.recaudo.application.dto.response.ResponseDTO;
import pe.com.integra.api.recaudo.infraestructure.util.Constantes;

public class SecurityInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest req, HttpServletResponse res, Object handler) throws Exception {
		String token = req.getHeader(Constantes.HEADER_USER);
		if (StringUtils.isNotBlank(token)) {
			return true;
		} else {
			ResponseDTO<Object> responseDTO = new ResponseDTO<>();
			responseDTO.setCode(Constantes.CODE_HEADER_USER_EMPTY);
			responseDTO.setStatus(false);
			responseDTO.setMessage(Constantes.MSG_HEADER_USER_EMPTY);
			responseDTO.setBody(new ArrayList<>());
			writeResponse(res, HttpStatus.BAD_REQUEST, responseDTO);
			return false;
		}
	}

	private void writeResponse(HttpServletResponse res, HttpStatus status, ResponseDTO<Object> exc) throws IOException {
		res.setStatus(status.value());
		res.setContentType(MediaType.APPLICATION_JSON_VALUE);
		ObjectMapper objectMapper = new ObjectMapper();
		PrintWriter writer = res.getWriter();
		writer.print(objectMapper.writeValueAsString(exc));
		writer.close();
	}
}