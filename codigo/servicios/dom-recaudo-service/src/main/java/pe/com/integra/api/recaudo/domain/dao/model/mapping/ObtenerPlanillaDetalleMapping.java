package pe.com.integra.api.recaudo.domain.dao.model.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;
import pe.com.integra.api.recaudo.infraestructure.util.Utils;

public class ObtenerPlanillaDetalleMapping implements RowMapper<PlanillaDetalle> {

	@Override
	public PlanillaDetalle mapRow(ResultSet rs, int arg1) throws SQLException {

		PlanillaDetalle obj = new PlanillaDetalle();
		obj.setPagina(rs.getBigDecimal("DETPAG"));
		obj.setSecuencia(rs.getBigDecimal("DETSEC"));
		obj.setCuspp(Utils.validString(rs.getString("DETNU2")));
		obj.setPrimerApellido(Utils.validString(rs.getString("DETPRI")));
		obj.setSegundoApellido(Utils.validString(rs.getString("DETSEG")));
		obj.setPrimerNombre(Utils.validString(rs.getString("DETPR1")));
		obj.setSegundoNombre(Utils.validString(rs.getString("DETSE1")));
		obj.setRemuneracionAsegurable(rs.getBigDecimal("DETREM"));
		obj.setCotizacionObligatoria(rs.getBigDecimal("DETCOT"));
		obj.setCotizacionCompleAfi(rs.getBigDecimal("DETC10"));
		obj.setCotizacionCompleEmp(rs.getBigDecimal("DETC11"));
		obj.setCotizacionConFinPre(rs.getBigDecimal("DETCO1"));
		obj.setCotizacionSinFinPre(rs.getBigDecimal("DETAHO"));
		obj.setCotizacionVolEmp(rs.getBigDecimal("DETCO2"));
		obj.setCotizacionCTS(rs.getBigDecimal("DETCTA"));
		obj.setSubtotalFondo(rs.getBigDecimal("DETSUB"));
		obj.setContribucionIPSS(rs.getBigDecimal("DETCO8"));
		obj.setSeguros(rs.getBigDecimal("DETSE4"));
		obj.setComision(rs.getBigDecimal("DETCOM"));
		obj.setComisionPorcentaje(rs.getBigDecimal("DETCO9"));
		obj.setSubtotalRetenciones(rs.getBigDecimal("DETSU1"));
		obj.setTotalDetalle(rs.getBigDecimal("DETTOT"));
		obj.setTipoMovimiento(rs.getBigDecimal("DETTIP"));
		obj.setFechaMovimiento(Utils.validString(rs.getString("DETFEC")));
		obj.setFechaFinalizacion(Utils.validString(rs.getString("DETFE1")));
		obj.setTipoIdentificacionSubsi(Utils.validString(rs.getString("DETTI3")));
		obj.setIdentificacionSubsi(Utils.validString(rs.getString("DETIDE")));
		obj.setValorConciliado(rs.getBigDecimal("DETVAL"));
		obj.setNumeroCuotasConciliadas(rs.getBigDecimal("DETCUO"));
		obj.setTipoFondo(Utils.validString(rs.getString("DETTI2")));
		obj.setUsuario(Utils.validString(rs.getString("DETUSU")));
		obj.setFechaModificacion(Utils.validString(rs.getString("DETFE4")));
		obj.setIndicadorUno(Utils.validString(rs.getString("DETIN6")));
		obj.setIndicadorDos(Utils.validString(rs.getString("DETIN7")));
		obj.setIndicadorTres(Utils.validString(rs.getString("DETIN8")));

		obj.setDevengue(Utils.validString(rs.getString("REP886")));
		obj.setSecuenciaCuota(Utils.validBigDecimalZero(rs.getBigDecimal("REP887")));
		obj.setInteresTardioFdo(Utils.validBigDecimalZero(rs.getBigDecimal("REP881")));
		obj.setInteresTardioSeg(Utils.validBigDecimalZero(rs.getBigDecimal("REP882")));
		obj.setInteresTardioCom(Utils.validBigDecimalZero(rs.getBigDecimal("REP883")));
		obj.setInteresFracFondo(Utils.validBigDecimalZero(rs.getBigDecimal("REP888")));
		obj.setInteresFracSeg(Utils.validBigDecimalZero(rs.getBigDecimal("REP889")));
		obj.setInteresFracCom(Utils.validBigDecimalZero(rs.getBigDecimal("REP890")));
		obj.setRentabilidadFondo(Utils.validBigDecimalZero(rs.getBigDecimal("REP891")));
		obj.setRentabilidadSeguro(Utils.validBigDecimalZero(rs.getBigDecimal("REP892")));
		obj.setRentabilidadComis(Utils.validBigDecimalZero(rs.getBigDecimal("REP893")));
		obj.setFinCuota(Utils.validString(rs.getString("REPFIN")));
		obj.setIndicadorUnoRep(Utils.validString(rs.getString("REP902")));
		obj.setIndicadorDosRep(Utils.validString(rs.getString("REP903")));
		obj.setFechaIngresoRep(Utils.validString(rs.getString("REP884")));
		obj.setUsuarioIngresoRep(Utils.validString(rs.getString("REPU47")));
		obj.setFechaModificacionRep(Utils.validString(rs.getString("REP885")));
		obj.setUsuarioModificacionRep(Utils.validString(rs.getString("REPU48")));

		obj.setAfpDetalle(Utils.validString(rs.getString("AFP")));
		
		return obj;
	}
}