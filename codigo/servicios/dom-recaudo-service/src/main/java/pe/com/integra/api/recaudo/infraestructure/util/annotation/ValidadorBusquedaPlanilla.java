package pe.com.integra.api.recaudo.infraestructure.util.annotation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDTO;

public class ValidadorBusquedaPlanilla
		implements ConstraintValidator<BusquedaPlanillaValida, RequestBusquedaPlanillaDTO> {

	@Override
	public void initialize(BusquedaPlanillaValida constraintAnnotation) {
		// Se sobreescribe el metodo por causar conflicto en Jboss
	}

	@Override
	public boolean isValid(RequestBusquedaPlanillaDTO request, ConstraintValidatorContext context) {
		StringBuilder mensaje = new StringBuilder();

		if (request.getLote() == null && (request.getNroPreimpreso() == null || request.getNroPreimpreso().isEmpty())
				&& (request.getPeriodoDevengue() == null || request.getPeriodoDevengue().isEmpty())
				&& request.getPlanillaInterno() == null) {
			context.disableDefaultConstraintViolation();
			mensaje.append("Un criterio de primer nivel es necesario. ");
		}

		if ((request.getTipoIdentificacion() != null && !request.getTipoIdentificacion().isEmpty())
				&& (request.getNumeroIdentificacion() == null || request.getNumeroIdentificacion().isEmpty())) {
			context.disableDefaultConstraintViolation();
			mensaje.append("Si ingreso el tipo de identificacion debe ingresar el numero de identificacion. ");
		}

		if ((request.getNumeroIdentificacion() != null && !request.getNumeroIdentificacion().isEmpty())
				&& (request.getTipoIdentificacion() == null || request.getTipoIdentificacion().isEmpty())) {
			context.disableDefaultConstraintViolation();

			mensaje.append("Si ingreso el numero de identificacion debe ingresar el tipo de identificacion. ");
		}

		if (!mensaje.toString().isEmpty()) {
			context.buildConstraintViolationWithTemplate(mensaje.toString()).addPropertyNode("")
					.addConstraintViolation();
			return false;
		}

		return true;
	}
}