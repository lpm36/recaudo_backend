package pe.com.integra.api.recaudo.application.dto.response;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "ResponseBusquedaPlanillasDTO", description = "respuesta listado de registros encontrados en la busqueda de planilla")
public class ResponseBusquedaPlanillasDTO {
	
	@ApiModelProperty(value = "cantitad de registros encontrados sin paginacion", required = false, position = 1)
	private BigDecimal cantidadTotalEncontrados;
	
	@ApiModelProperty(value = "Listado de planillas encontradas", required = false, position = 2)
	private List<ResponsePlanillaDTO> planillaCabeceras; 
}