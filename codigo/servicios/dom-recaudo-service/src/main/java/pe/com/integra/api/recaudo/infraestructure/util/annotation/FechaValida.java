package pe.com.integra.api.recaudo.infraestructure.util.annotation;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ FIELD })
@Retention(RUNTIME)
@Constraint(validatedBy = ValidadorFecha.class)
public @interface FechaValida {
	String message() default "Fecha con formato incorrecto";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

	String formato() default "yyyyMMdd";
}
