package pe.com.integra.api.recaudo.application.dto.request;

import java.math.BigDecimal;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import pe.com.integra.api.recaudo.infraestructure.util.annotation.BusquedaPlanillaDetalleValida;
import pe.com.integra.api.recaudo.infraestructure.util.annotation.FechaValida;

@Getter
@Setter
@ToString
@BusquedaPlanillaDetalleValida
@ApiModel(value = "RequestBusquedaPlanillaDetalleDTO", description = "parametros de entrada para la busqueda de planilla")
public class RequestBusquedaPlanillaDetalleDTO{

	@Min(value=1L, message = "debe ingresar un numero entero positivo")
	@Max(value = 9999999L, message = "longitud maxima de 7 digitos")
	@ApiModelProperty(value = "lote de planilla(ejem: 7019573).", required = true, position = 1)
	private BigDecimal lote;

	@Min(value=1L, message = "debe ingresar un numero entero positivo")
	@Max(value = 999999999L, message = "longitud maxima de 9 digitos")
	@ApiModelProperty(value = "numero planilla interno", required = false, position = 2)
	private BigDecimal planillaInterno;

	@Size(min = 6, max = 6, message = "ingresar periodo valido con formato YYYYMM")
	@FechaValida(message = "ingresar periodo valido con formato YYYYMM", formato = "yyyyMM")
	@ApiModelProperty(value = "periodo de devengue (formato: YYYYMM)').", required = false, position = 7)
	private String periodoDevengue;

	@Size(max = 3, message = "Debe tener como maximo 3 caracteres de longitud")
	@ApiModelProperty(value = "tipo de documento/identificacion (maximo 3 caracteres)').", required = false, position = 8)
	private String tipoIdentificacion;

	@Size(max = 16, message = "Debe tener como maximo 16 caracteres de longitud")
	@ApiModelProperty(value = "numero de documento/identificacion (maximo 16 caracteres)').", required = false, position = 9)
	private String numeroIdentificacion;

	@Size(max = 16, message = "Debe tener como maximo 16 caracteres de longitud")
	@ApiModelProperty(value = "CUSPP (maximo 16 caracteres)').", required = false, position = 10)
	private String cuspp;

	@Min(value=1L, message = "debe ingresar un numero entero positivo")
	@NotNull(message = "el registro inicial de paginacion es obligatorio")
	@ApiModelProperty(value = "registro inicial para paginacion').", required = true, position = 12)
	private Integer registroInicial;

	@Min(value=1L, message = "debe ingresar un numero entero positivo")
	@NotNull(message = "el registro final de paginacion es obligatorio")
	@ApiModelProperty(value = "registro final para paginacion", required = true, position = 13)
	private Integer registroFinal;
}