package pe.com.integra.api.recaudo.domain.dao.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import lombok.extern.slf4j.Slf4j;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDTO;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.domain.dao.PlanillaDao;
import pe.com.integra.api.recaudo.domain.dao.model.Planilla;
import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;
import pe.com.integra.api.recaudo.domain.dao.model.mapping.ObtenerPlanillaCabeceraMapping;
import pe.com.integra.api.recaudo.domain.dao.model.mapping.BusquedaPlanillaDetalleMapping;
import pe.com.integra.api.recaudo.domain.dao.model.mapping.BusquedaPlanillaCabeceraMapping;
import pe.com.integra.api.recaudo.domain.dao.model.mapping.ObtenerPlanillaDetalleMapping;
import pe.com.integra.api.recaudo.infraestructure.util.Constantes;
import pe.com.integra.api.recaudo.infraestructure.util.UtilDatabase;

@Slf4j
@Repository
public class PlanillaDaoImpl implements PlanillaDao {

	@Autowired
	@Qualifier("as400JdbcTemplate")
	private JdbcTemplate as400JdbcTemplate;

	@Autowired
	private UtilDatabase utilDatabase;

	@Override
	public Map<String, Object> buscarPlanillas(RequestBusquedaPlanillaDTO request) {
		log.info("Ini buscarPlanillas: request{}", request.toString());
		SimpleJdbcCall jdbcDb2 = utilDatabase.getSimpleDb2(as400JdbcTemplate, Constantes.SP_BUSCARPLANILLA);
		jdbcDb2.returningResultSet(Constantes.MAPPER_PLANILLA_BUS, new BusquedaPlanillaCabeceraMapping());
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue(Constantes.P_LOTE, request.getLote());
		sqlParameter.addValue(Constantes.P_PLANILLA_INTERNO, request.getPlanillaInterno());
		sqlParameter.addValue(Constantes.P_NRO_PREIMPRESO, request.getNroPreimpreso());
		sqlParameter.addValue(Constantes.P_TIPO_PLANILLA, request.getTipoPlanilla());
		sqlParameter.addValue(Constantes.P_ESTADO_PLANILLA, request.getEstadoPlanilla());
		sqlParameter.addValue(Constantes.P_ESTADO_CONCILIACION, request.getEstadoConciliacion());
		sqlParameter.addValue(Constantes.P_PERIODO_DEVENGUE, request.getPeriodoDevengue());
		sqlParameter.addValue(Constantes.P_TIPO_IDENTIFICACION, request.getTipoIdentificacion());
		sqlParameter.addValue(Constantes.P_NUMERO_IDENTIFICACION, request.getNumeroIdentificacion());
		sqlParameter.addValue(Constantes.P_FECHA_PAGO_INICIAL, request.getFechaPagoInicial());
		sqlParameter.addValue(Constantes.P_FECHA_PAGO_FINAL, request.getFechaPagoFinal());
		sqlParameter.addValue(Constantes.P_REGINI, request.getRegistroInicial());
		sqlParameter.addValue(Constantes.P_REGFIN, request.getRegistroFinal());

		Map<String, Object> result = jdbcDb2.execute(sqlParameter);

		log.info("Fin buscarPlanillas: request{}", request.toString());
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Planilla> obtenerPlanilla(BigDecimal lotePlanilla, BigDecimal numeroPlanilla) {
		log.info("Ini obtenerPlanilla: numeroPlanilla{}", numeroPlanilla);
		
		SimpleJdbcCall jdbcDb2 = utilDatabase.getSimpleDb2(as400JdbcTemplate, Constantes.SP_OBTENERPLANILLA);
		jdbcDb2.returningResultSet(Constantes.MAPPER_PLANILLA_CAB, new ObtenerPlanillaCabeceraMapping());

		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue(Constantes.P_LOTE, lotePlanilla);
		sqlParameter.addValue(Constantes.P_PLANILLA_INTERNO, numeroPlanilla);

		Map<String, Object> result = jdbcDb2.execute(sqlParameter);

		log.info("Fin obtenerPlanilla: numeroPlanilla {}", numeroPlanilla);
		
		return (List<Planilla>) result.get(Constantes.MAPPER_PLANILLA_CAB);
	}

	@Override
	public Map<String, Object> buscarPlanillasDetalles(RequestBusquedaPlanillaDetalleDTO request) {
		log.info("Ini buscarPlanillasDetalles: request{}", request.toString());
		
		SimpleJdbcCall jdbcDb2 = utilDatabase.getSimpleDb2(as400JdbcTemplate, Constantes.SP_BUSCARDETALLE);
		jdbcDb2.returningResultSet(Constantes.MAPPER_PLANILLA_DET, new BusquedaPlanillaDetalleMapping());

		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue(Constantes.P_LOTE, request.getLote());
		sqlParameter.addValue(Constantes.P_PLANILLA_INTERNO, request.getPlanillaInterno());
		sqlParameter.addValue(Constantes.P_PERIODO_DEVENGUE, request.getPeriodoDevengue());
		sqlParameter.addValue(Constantes.P_CUSPP, request.getCuspp());
		sqlParameter.addValue(Constantes.P_TIPO_IDENTIFICACION, request.getTipoIdentificacion());
		sqlParameter.addValue(Constantes.P_NUMERO_IDENTIFICACION, request.getNumeroIdentificacion());
		sqlParameter.addValue(Constantes.P_REGINI, request.getRegistroInicial());
		sqlParameter.addValue(Constantes.P_REGFIN, request.getRegistroFinal());

		Map<String, Object> result = jdbcDb2.execute(sqlParameter);

		log.info("Fin buscarPlanillasDetalles: request{}", request.toString());
		
		return result;
	}

	@Override
	public Map<String, Object> obtenerDetallePlanilla(PlanillaDetalle parametrosDetalle) {
		log.info("Ini obtenerDetallePlanilla: parametrosDetalle{}", parametrosDetalle.toString());
		
		SimpleJdbcCall jdbcDb2 = utilDatabase.getSimpleDb2(as400JdbcTemplate, Constantes.SP_OBTENERDETALLE);
		jdbcDb2.returningResultSet(Constantes.MAPPER_PLANILLA_BUS, new BusquedaPlanillaCabeceraMapping());
		jdbcDb2.returningResultSet(Constantes.MAPPER_OBTENER_PLANILLA_DETALLE, new ObtenerPlanillaDetalleMapping());

		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue(Constantes.P_LOTE, parametrosDetalle.getLote());
		sqlParameter.addValue(Constantes.P_PLANILLA_INTERNO, parametrosDetalle.getPlanillaInterno());
		sqlParameter.addValue(Constantes.P_PAGINA, parametrosDetalle.getPagina());
		sqlParameter.addValue(Constantes.P_SECUENCIA, parametrosDetalle.getSecuencia());

		Map<String, Object> result = jdbcDb2.execute(sqlParameter);

		log.info("Fin obtenerDetallePlanilla: parametrosDetalle{}", parametrosDetalle.toString());
		
		return result;
	}
}