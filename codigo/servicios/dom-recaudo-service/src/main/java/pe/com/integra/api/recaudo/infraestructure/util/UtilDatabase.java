package pe.com.integra.api.recaudo.infraestructure.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;

import pe.com.integra.api.recaudo.domain.dao.model.SecretAWS;

@Component
public class UtilDatabase {

	@Value("${schema.store.procedure}")
//	@Value("MINTPRO")
	private String schemaSP;
	
	@Autowired
	@Qualifier("secretsAWS") 
	SecretAWS secretAWS;

	public SimpleJdbcCall getSimpleDb2(JdbcTemplate jdbcTemplateDb2, String nameSpDb2) {
		try {
			SimpleJdbcCall jdbcDb2 = new SimpleJdbcCall(jdbcTemplateDb2);
			jdbcDb2.withProcedureName(nameSpDb2);
			schemaSP = (schemaSP == null? secretAWS.getSchema(): schemaSP);
			jdbcDb2.withSchemaName(schemaSP);
			return jdbcDb2;
		} catch (Exception ex) {
			return null;
		}
	}

}
