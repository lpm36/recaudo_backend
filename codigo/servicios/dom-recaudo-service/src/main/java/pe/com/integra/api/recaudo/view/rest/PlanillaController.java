package pe.com.integra.api.recaudo.view.rest;

import java.math.BigDecimal;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import pe.com.integra.api.recaudo.application.PlanillaService;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDTO;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseObtenerCabeceraPlanillasDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseBusquedaPlanillasDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseBusquedaPlanillasDetalleDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseObtenerPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;
import pe.com.integra.api.recaudo.infraestructure.util.Utils;

@Slf4j
@RestController
@Validated
@RequestMapping("/api")
@Api(tags = { "Planillas" }, value = "/api", produces = "application/json")
public class PlanillaController {

	@Autowired
	PlanillaService planillaService;

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Servicio que obtiene el listado de cabeceras de planillas segun criterio de busqueda")
	@PostMapping(value = "/planilla/busqueda")
	public ResponseEntity<ResponseDTO<ResponseBusquedaPlanillasDTO>> buscarPlanillas(
			@RequestHeader(value = "usuario") String usuario,
			@RequestHeader(value = "version", required = false) String version,
			@Valid @RequestBody RequestBusquedaPlanillaDTO request) {

		log.info("Ini buscarPlanillas: usuario {}", usuario);

		ResponseBusquedaPlanillasDTO busqueda = planillaService.buscarPlanillas(request);

		ResponseDTO<ResponseBusquedaPlanillasDTO> responseDTO = Utils.returnResponseSuccessful(busqueda);

		log.info("Fin buscarPlanillas: usuario {}", usuario);

		return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Servicio que obtiene la cabecera de una planilla")
	@GetMapping(value = "/planilla/{lote_planilla}-{numero_planilla}")
	public ResponseEntity<ResponseDTO<ResponseObtenerCabeceraPlanillasDTO>> obtenerCabeceraPlanilla(
			@RequestHeader(value = "usuario", required = false) String usuario,
			@RequestHeader(value = "version", required = false) String version,
			@PathVariable(name = "lote_planilla") @Min(value = 1L, message = "debe ingresar un numero positivo") @Max(value = 9999999L, message = "longitud maxima de 7 digitos") BigDecimal lotePlanilla,
			@PathVariable(name = "numero_planilla") @Min(value = 1L, message = "debe ingresar un numero positivo") @Max(value = 999999999L, message = "longitud maxima de 9 digitos") BigDecimal numeroPlanilla) {

		log.info("Ini obtenerCabeceraPlanilla: usuario {}", usuario);

		ResponseObtenerCabeceraPlanillasDTO dto = planillaService.obtenerCabeceraPlanilla(usuario, lotePlanilla,
				numeroPlanilla);

		ResponseDTO<ResponseObtenerCabeceraPlanillasDTO> responseDTO = Utils.returnResponseSuccessful(dto);

		log.info("Fin obtenerCabeceraPlanilla: usuario {}", usuario);

		return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Servicio que obtiene el listado de detalles de una planilla segun criterio de busqueda")
	@PostMapping(value = "/planilla/busquedadetalle")
	public ResponseEntity<ResponseDTO<ResponseBusquedaPlanillasDetalleDTO>> buscarPlanillasDetalles(
			@RequestHeader(value = "usuario") String usuario,
			@RequestHeader(value = "version", required = false) String version,
			@Valid @RequestBody RequestBusquedaPlanillaDetalleDTO request) {

		log.info("Ini buscarPlanillasDetalles: usuario {}", usuario);

		ResponseBusquedaPlanillasDetalleDTO dto = planillaService.buscarPlanillasDetalles(request);

		ResponseDTO<ResponseBusquedaPlanillasDetalleDTO> responseDTO = Utils.returnResponseSuccessful(dto);

		log.info("Fin buscarPlanillasDetalles: usuario {}", usuario);

		return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
	}

	@SuppressWarnings("unchecked")
	@ApiOperation(value = "Servicio que obtiene la cabecera y detalle de una planilla")
	@GetMapping(value = "/planilla/{lote_planilla}-{numero_planilla}/detalle/{pagina}-{secuencia}")
	public ResponseEntity<ResponseDTO<ResponseObtenerPlanillaDetalleDTO>> obtenerDetallePlanilla(
			@RequestHeader(value = "usuario") String usuario,
			@RequestHeader(value = "version", required = false) String version,
			@PathVariable(name = "lote_planilla") @Min(value = 1L, message = "debe ingresar un numero positivo") @Max(value = 9999999L, message = "longitud maxima de 7 digitos") BigDecimal lotePlanilla,
			@PathVariable(name = "numero_planilla") @Min(value = 1L, message = "debe ingresar un numero positivo") @Max(value = 999999999L, message = "longitud maxima de 9 digitos") BigDecimal numeroPlanilla,
			@PathVariable(name = "pagina") @Min(value = 1L, message = "debe ingresar un numero positivo") BigDecimal pagina,
			@PathVariable(name = "secuencia") @Min(value = 1L, message = "debe ingresar un numero positivo") BigDecimal secuencia) {

		log.info("Ini obtenerDetallePlanilla: usuario {}", usuario);

		PlanillaDetalle parametrosDetalle = new PlanillaDetalle();

		parametrosDetalle.setLote(lotePlanilla);
		parametrosDetalle.setPlanillaInterno(numeroPlanilla);
		parametrosDetalle.setPagina(pagina);
		parametrosDetalle.setSecuencia(secuencia);

		ResponseObtenerPlanillaDetalleDTO dto = planillaService.obtenerDetallePlanilla(parametrosDetalle);
		ResponseDTO<ResponseObtenerPlanillaDetalleDTO> responseDTO = Utils.returnResponseSuccessful(dto);

		log.info("Fin obtenerDetallePlanilla: usuario {}", usuario);

		return ResponseEntity.status(HttpStatus.OK).body(responseDTO);
	}
}