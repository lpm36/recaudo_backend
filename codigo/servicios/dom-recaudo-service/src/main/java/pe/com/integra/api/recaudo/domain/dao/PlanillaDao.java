package pe.com.integra.api.recaudo.domain.dao;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDTO;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.domain.dao.model.Planilla;
import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;

public interface PlanillaDao {

	public Map<String, Object> buscarPlanillas(RequestBusquedaPlanillaDTO request);

	public List<Planilla> obtenerPlanilla(BigDecimal lotePlanilla, BigDecimal numeroPlanilla);

	public Map<String, Object> buscarPlanillasDetalles(RequestBusquedaPlanillaDetalleDTO request);

	public Map<String, Object> obtenerDetallePlanilla(PlanillaDetalle parametrosDetalle);

}
