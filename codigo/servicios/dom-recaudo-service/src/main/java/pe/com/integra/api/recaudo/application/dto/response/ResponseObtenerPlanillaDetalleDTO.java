package pe.com.integra.api.recaudo.application.dto.response;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "ResponsePlanillaDetalleDTO", description = "respuesta listado de detalles de planilla.")
public class ResponseObtenerPlanillaDetalleDTO extends ResponsePlanillaDetalleDTO{

	@ApiModelProperty(value = "cotiz.obl.comple.afi", position = 24)
	private BigDecimal cotizacionCompleAfi;

	@ApiModelProperty(value = "cotiz.obligatoria complemen", position = 25)
	private BigDecimal cotizacionCompleEmp;

	@ApiModelProperty(value = "cot.vol.sin fin prev", position = 26)
	private BigDecimal cotizacionSinFinPre;

	@ApiModelProperty(value = "cot.vol.con fin prev", position = 27)
	private BigDecimal cotizacionConFinPre;

	@ApiModelProperty(value = "cotizacion voluntaria emple", position = 28)
	private BigDecimal cotizacionVolEmp;

	@ApiModelProperty(value = "cta.comp.tiempo serv", position = 29)
	private BigDecimal cotizacionCTS;

	@ApiModelProperty(value = "contribucion al IPSS", position = 30)
	private BigDecimal contribucionIPSS;

	@ApiModelProperty(value = "comision porcentual a pagar", position = 31)
	private BigDecimal comisionPorcentaje;

	@ApiModelProperty(value = "total consignado detalle", position = 32)
	private BigDecimal totalDetalle;

	@ApiModelProperty(value = "fecha finalizacion", position = 33)
	private String fechaFinalizacion;

	@ApiModelProperty(value = "tipo identif subsidio", position = 34)
	private String tipoIdentificacionSubsi;

	@ApiModelProperty(value = "identificador subsidio", position = 35)
	private String identificacionSubsi;
	

	@ApiModelProperty(value = "devengue", position = 36)
	private String devengue;

	@ApiModelProperty(value = "secuencia cuota", position = 37)
	private BigDecimal secuenciaCuota;

	@ApiModelProperty(value = "interes tardio fondo", position = 38)
	private BigDecimal interesTardioFdo;

	@ApiModelProperty(value = "interes tardio seguro", position = 39)
	private BigDecimal interesTardioSeg;

	@ApiModelProperty(value = "interes tardio comision", position = 40)
	private BigDecimal interesTardioCom;

	@ApiModelProperty(value = "interes frac fondo", position = 41)
	private BigDecimal interesFracFondo;

	@ApiModelProperty(value = "interes frac seg", position = 42)
	private BigDecimal interesFracSeg;

	@ApiModelProperty(value = "interes frac com", position = 43)
	private BigDecimal interesFracCom;

	@ApiModelProperty(value = "rentabilidad fondo", position = 44)
	private BigDecimal rentabilidadFondo;

	@ApiModelProperty(value = "rentabilidad seguro", position = 45)
	private BigDecimal rentabilidadSeguro;

	@ApiModelProperty(value = "rentabilidad comision", position =46)
	private BigDecimal rentabilidadComis;

	@ApiModelProperty(value = "fin de cuota", position = 47)
	private String finCuota;

	@ApiModelProperty(value = "indicador 1", position = 48)
	private String indicadorUnoRep;

	@ApiModelProperty(value = "indicador 2", position = 49)
	private String indicadorDosRep;

	@ApiModelProperty(value = "fecha ingreso", position = 50)
	private String fechaIngresoRep;

	@ApiModelProperty(value = "usuario ingreso", position = 51)
	private String usuarioIngresoRep;

	@ApiModelProperty(value = "fecha de modificacion", position = 52)
	private String fechaModificacionRep;

	@ApiModelProperty(value = "usuario de modificacion", position = 53)
	private String usuarioModificacionRep;
}
