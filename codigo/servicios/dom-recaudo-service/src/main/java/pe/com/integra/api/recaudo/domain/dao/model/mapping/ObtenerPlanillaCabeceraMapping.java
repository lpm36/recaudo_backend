package pe.com.integra.api.recaudo.domain.dao.model.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.integra.api.recaudo.domain.dao.model.Planilla;
import pe.com.integra.api.recaudo.infraestructure.util.Utils;

public class ObtenerPlanillaCabeceraMapping implements RowMapper<Planilla> {

	@Override
	public Planilla mapRow(ResultSet rs, int arg1) throws SQLException {

		Planilla objCab = new Planilla();
		objCab.setLote(rs.getBigDecimal("PLANU3"));
		objCab.setPlanillaFisico(rs.getBigDecimal("PLANUM"));
		objCab.setPlanillaInterno(rs.getBigDecimal("PLANU1"));
		objCab.setNroPreimpreso(Utils.validString(rs.getString("PLAN01")));
		objCab.setMedioMagnetico(Utils.validString(rs.getString("PLAR33")));
		objCab.setTipoFormulario(Utils.validString(rs.getString("PLAT23")));
		objCab.setTipoPlanilla(Utils.validString(rs.getString("PLATIP")));
		objCab.setDescTipoPlanilla(Utils.validString(rs.getString("ZTIPPL")));
		objCab.setEstadoPlanilla(Utils.validString(rs.getString("PLAEST")));
		objCab.setEstadoConciliacion(Utils.validString(rs.getString("PLAES4")));
		objCab.setTipoPagoPlanilla(Utils.validString(rs.getString("PLAT03")));
		objCab.setPeriodoDevengue(Utils.validString(rs.getString("PLAPER")));
		objCab.setFechaPago(Utils.validString(rs.getString("PLAFEC")));
		objCab.setCodigoEntidadRec(Utils.validString(rs.getString("PLACO2")));
		objCab.setNombreEntidadRec(Utils.validString(rs.getString("ZENTID")));
		objCab.setTipoIdentificacion(Utils.validString(rs.getString("PLATI1")));
		objCab.setNumeroIdentificacion(Utils.validString(rs.getString("PLANU4")));
		objCab.setTotalAfiliados(rs.getBigDecimal("PLAT04"));
		objCab.setTotalRemuneraciones(rs.getBigDecimal("PLATOT"));
		objCab.setTotalCotizacionObligatorio(rs.getBigDecimal("PLAT05"));
		objCab.setSubtotalFondoPensiones(rs.getBigDecimal("PLASUB"));
		objCab.setPenalidadesFondo(rs.getBigDecimal("PLACOS"));
		objCab.setTotalPagarFondo(rs.getBigDecimal("PLATO4"));
		objCab.setSumaAportesInformados(rs.getBigDecimal("PLATO5"));
		objCab.setSumaAportesFondoCalculados(rs.getBigDecimal("PLAV07"));
		objCab.setValorConciliadoFondo(rs.getBigDecimal("PLAVAL"));
		objCab.setNroCuotasConciliadas(rs.getBigDecimal("PLAN03"));
		objCab.setValorPagarEfectivoFondo(rs.getBigDecimal("PLAVA4"));
		objCab.setPlanillaAPagar(rs.getBigDecimal("PLAPLA"));
		objCab.setIndicadorForzarAcreditacion(Utils.validString(rs.getString("PLAI06")));
		objCab.setIndicadorDos(Utils.validString(rs.getString("PLAI07")));
		objCab.setIndicadorTres(Utils.validString(rs.getString("PLAI08")));
		objCab.setDescTipoAccion(Utils.validString(rs.getString("ZPLAT6")));
		objCab.setNombreEmpleador(Utils.validString(rs.getString("PLANOM")));
		objCab.setGiroNegocio(Utils.validString(rs.getString("PLAGIR")));
		objCab.setSucursalEmpleador(Utils.validString(rs.getString("PLASUC")));
		objCab.setDireccionEmpleador(Utils.validString(rs.getString("PLADIR")));
		objCab.setNumeroDepartamento(Utils.validString(rs.getString("PLANR3")));
		objCab.setCodigoPostalEmpleador(Utils.validString(rs.getString("PLACO1")));
		objCab.setDistritoEmpleador(Utils.validString(rs.getString("PLADIS")));
		objCab.setCiudadEmpleador(Utils.validString(rs.getString("PLACIU")));
		objCab.setFechaModificacion(Utils.validString(rs.getString("PLAFE1")));
		objCab.setDepartamentoEmpleador(Utils.validString(rs.getString("PLADEP")));
		objCab.setUsuarioModificacion(Utils.validString(rs.getString("PLAUSU")));
		objCab.setUrbanizacionEmpleador(Utils.validString(rs.getString("PLAURB")));
		objCab.setCodigoCiudadEmpleador(Utils.validString(rs.getString("PLAC63")));
		objCab.setTelefonoEmpleador(Utils.validString(rs.getString("PLATEL")));
		objCab.setFusionado(Utils.validString(rs.getString("PMSG")));
		objCab.setNombreElaborador(Utils.validString(rs.getString("PLANO1")));
		objCab.setAreaEmpresa(Utils.validString(rs.getString("PLAA41")));
		objCab.setTelefonoContacto(Utils.validString(rs.getString("PLATE5")));
		objCab.setAnexoContacto(Utils.validString(rs.getString("PLAANE")));
		objCab.setNumeroLiquidacion(Utils.validString(rs.getString("PLALQC")));
		objCab.setRegularizaPlanilla(Utils.validString(rs.getString("PLAPL9")));
		objCab.setTotalObligatorioAfiliado(rs.getBigDecimal("PLATO9"));
		objCab.setTotalObligatorioEmpleador(rs.getBigDecimal("PLAT01"));
		objCab.setTotalCotConFinPrevisional(rs.getBigDecimal("PLATO1"));
		objCab.setTotalCotSinFinPrevisional(rs.getBigDecimal("PLATO2"));
		objCab.setTotalCotVoluntaria(rs.getBigDecimal("PLAT06"));
		objCab.setTotalCts(rs.getBigDecimal("PLATO3"));
		objCab.setInteresesFondo(rs.getBigDecimal("PLAREC"));
		objCab.setDescEstadoPlanilla(Utils.validString(rs.getString("ZESTAD")));
		objCab.setDescEstadoConciliacion(Utils.validString(rs.getString("ZCONCI")));
		objCab.setDescTipoPagoPlanilla(Utils.validString(rs.getString("TIPPAG")));
		objCab.setNumeroHojasAdicionales(rs.getBigDecimal("PLAHJA"));
		objCab.setFormaPagoFondo(Utils.validString(rs.getString("PLAFOR")));
		objCab.setDescFormaPagoFondo(Utils.validString(rs.getString("ZTIPPA")));
		objCab.setNumeroChequePago(Utils.validString(rs.getString("PLANR8")));
		objCab.setBancoPago(Utils.validString(rs.getString("PLABAN")));
		objCab.setGrupoRiesgo(Utils.validString(rs.getString("PLAGRU")));
		objCab.setTipoAccion(Utils.validString(rs.getString("PLATI6")));
		objCab.setSumaDetalles(rs.getBigDecimal("ZSUMDF"));
		objCab.setIntAsumeAfp(Utils.validBigDecimalZero(rs.getBigDecimal("AFPINT")));
		objCab.setObservacion(Utils.validString(rs.getString("WOBSER")));
		
		objCab.setAfp(Utils.validString(rs.getString("AFP")));
		
		return objCab;
	}
}