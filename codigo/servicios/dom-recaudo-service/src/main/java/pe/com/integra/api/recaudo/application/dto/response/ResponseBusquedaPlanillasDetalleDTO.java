package pe.com.integra.api.recaudo.application.dto.response;

import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "ResponseBusquedaPlanillasDetalleDTO", description = "respuesta de obtencion de detalles de planilla")
public class ResponseBusquedaPlanillasDetalleDTO{

	@ApiModelProperty(value = "cantitad de registros encontrados sin paginacion", required = false, position = 1)
	private BigDecimal cantidadTotalEncontrados;
	
	@ApiModelProperty(value = "listado de detalles DETARC", position = 29)
	private List<ResponsePlanillaDetalleDTO> planillaDetalles;
}