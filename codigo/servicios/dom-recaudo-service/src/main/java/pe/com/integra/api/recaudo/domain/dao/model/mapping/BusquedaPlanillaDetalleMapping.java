package pe.com.integra.api.recaudo.domain.dao.model.mapping;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;
import pe.com.integra.api.recaudo.infraestructure.util.Utils;

public class BusquedaPlanillaDetalleMapping implements RowMapper<PlanillaDetalle> {

	@Override
	public PlanillaDetalle mapRow(ResultSet rs, int arg1) throws SQLException {

		PlanillaDetalle objPlanillaDetalle = new PlanillaDetalle();

		objPlanillaDetalle.setLote(rs.getBigDecimal("PLANU3"));
		objPlanillaDetalle.setPlanillaFisico(rs.getBigDecimal("PLANUM"));
		objPlanillaDetalle.setPlanillaInterno(rs.getBigDecimal("PLANU1"));
		objPlanillaDetalle.setNroPreimpreso(Utils.validString(rs.getString("PLAN01")));
		objPlanillaDetalle.setTipoPlanilla(Utils.validString(rs.getString("PLATIP")));
		objPlanillaDetalle.setEstadoPlanilla(Utils.validString(rs.getString("PLAEST")));
		objPlanillaDetalle.setEstadoConciliacion(Utils.validString(rs.getString("PLAES4")));
		objPlanillaDetalle.setTipoPagoPlanilla(Utils.validString(rs.getString("PLAT03")));
		objPlanillaDetalle.setPeriodoDevengue(Utils.validString(rs.getString("PLAPER")));
		objPlanillaDetalle.setFechaPago(Utils.validString(rs.getString("PLAFEC")));
		objPlanillaDetalle.setCodigoEntidadRec(Utils.validString(rs.getString("PLACO2")));
		objPlanillaDetalle.setTipoIdentificacion(Utils.validString(rs.getString("PLATI1")));
		objPlanillaDetalle.setNumeroIdentificacion(Utils.validString(rs.getString("PLANU4")));

		objPlanillaDetalle.setPagina(rs.getBigDecimal("DETPAG"));
		objPlanillaDetalle.setSecuencia(rs.getBigDecimal("DETSEC"));
		objPlanillaDetalle.setCuspp(Utils.validString(rs.getString("DETNU2")));
		objPlanillaDetalle.setPrimerApellido(Utils.validString(rs.getString("DETPRI")));
		objPlanillaDetalle.setSegundoApellido(Utils.validString(rs.getString("DETSEG")));
		objPlanillaDetalle.setPrimerNombre(Utils.validString(rs.getString("DETPR1")));
		objPlanillaDetalle.setSegundoNombre(Utils.validString(rs.getString("DETSE1")));
		objPlanillaDetalle.setRemuneracionAsegurable(rs.getBigDecimal("DETREM"));
		objPlanillaDetalle.setCotizacionObligatoria(rs.getBigDecimal("DETCOT"));
		objPlanillaDetalle.setSubtotalFondo(rs.getBigDecimal("DETSUB"));
		objPlanillaDetalle.setSeguros(rs.getBigDecimal("DETSE4"));
		objPlanillaDetalle.setComision(rs.getBigDecimal("DETCOM"));
		objPlanillaDetalle.setSubtotalRetenciones(rs.getBigDecimal("DETSU1"));
		objPlanillaDetalle.setTipoMovimiento(rs.getBigDecimal("DETTIP"));
		objPlanillaDetalle.setFechaMovimiento(Utils.validString(rs.getString("DETFEC")));
		objPlanillaDetalle.setValorConciliado(rs.getBigDecimal("DETVAL"));
		objPlanillaDetalle.setNumeroCuotasConciliadas(rs.getBigDecimal("DETCUO"));
		objPlanillaDetalle.setTipoFondo(Utils.validString(rs.getString("DETTI2")));
		objPlanillaDetalle.setUsuario(Utils.validString(rs.getString("DETUSU")));
		objPlanillaDetalle.setFechaModificacion(Utils.validString(rs.getString("DETFE4")));
		objPlanillaDetalle.setIndicadorUno(Utils.validString(rs.getString("DETIN6")));
		objPlanillaDetalle.setIndicadorDos(Utils.validString(rs.getString("DETIN7")));
		objPlanillaDetalle.setIndicadorTres(Utils.validString(rs.getString("DETIN8")));

		objPlanillaDetalle.setAfpDetalle(Utils.validString(rs.getString("AFP")));

		return objPlanillaDetalle;
	}
}