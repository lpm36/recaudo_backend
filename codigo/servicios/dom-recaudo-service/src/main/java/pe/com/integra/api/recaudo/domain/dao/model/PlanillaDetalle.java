package pe.com.integra.api.recaudo.domain.dao.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PlanillaDetalle extends PlanillaCabeceraResumen{

	private BigDecimal pagina;
	private BigDecimal secuencia;
	private String cuspp;
	private String primerApellido;
	private String segundoApellido;
	private String primerNombre;
	private String segundoNombre;
	private BigDecimal remuneracionAsegurable;
	private BigDecimal cotizacionObligatoria;
	private BigDecimal subtotalFondo;
	private BigDecimal seguros;
	private BigDecimal comision;
	private BigDecimal subtotalRetenciones;
	private BigDecimal tipoMovimiento;
	private String fechaMovimiento;
	private BigDecimal valorConciliado;
	private BigDecimal numeroCuotasConciliadas;
	private String tipoFondo;
	private String usuario;
	private String fechaModificacion;
	private String indicadorUno;
	private String indicadorDos;
	private String indicadorTres;
	private BigDecimal cotizacionCompleAfi;
	private BigDecimal cotizacionCompleEmp;
	private BigDecimal cotizacionSinFinPre;
	private BigDecimal cotizacionConFinPre;
	private BigDecimal cotizacionVolEmp;
	private BigDecimal cotizacionCTS;
	private BigDecimal contribucionIPSS;
	private BigDecimal comisionPorcentaje;
	private BigDecimal totalDetalle;
	private String fechaFinalizacion;
	private String tipoIdentificacionSubsi;
	private String identificacionSubsi;
	
	private String devengue;
	private BigDecimal secuenciaCuota;
	private BigDecimal interesTardioFdo;
	private BigDecimal interesTardioSeg;
	private BigDecimal interesTardioCom;
	private BigDecimal interesFracFondo;
	private BigDecimal interesFracSeg;
	private BigDecimal interesFracCom;
	private BigDecimal rentabilidadFondo;
	private BigDecimal rentabilidadSeguro;
	private BigDecimal rentabilidadComis;
	private String finCuota;
	private String indicadorUnoRep;
	private String indicadorDosRep;
	private String fechaIngresoRep;
	private String usuarioIngresoRep;
	private String fechaModificacionRep;
	private String usuarioModificacionRep;
	
	private String afpDetalle;
}