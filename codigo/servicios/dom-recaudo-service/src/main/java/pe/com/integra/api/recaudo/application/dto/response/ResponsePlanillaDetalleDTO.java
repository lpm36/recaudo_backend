package pe.com.integra.api.recaudo.application.dto.response;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "ResponsePlanillaDetalleDTO", description = "respuesta listado de detalles de planilla.")
public class ResponsePlanillaDetalleDTO extends ResponsePlanillaDTO{
	
	@ApiModelProperty(value = "pagina", position = 1)
	private BigDecimal pagina;
	
	@ApiModelProperty(value = "secuencia", position = 2)
	private BigDecimal secuencia;

	@ApiModelProperty(value = "codigo unico de identificacion CUSPP", position = 3)
	private String cuspp;
	
	@ApiModelProperty(value = "primer apellido en el detalle", position = 4)
	private String primerApellido;

	@ApiModelProperty(value = "segundo apellido en el detalle", position = 5)
	private String segundoApellido;

	@ApiModelProperty(value = "primer nombre en el detalle", position = 6)
	private String primerNombre;

	@ApiModelProperty(value = "segundo nombre en el detalle", position = 7)
	private String segundoNombre;

	@ApiModelProperty(value = "remuneracion asegurable", position = 8)
	private BigDecimal remuneracionAsegurable;
	
	@ApiModelProperty(value = "cotizacion obligatoria", position = 9)
	private BigDecimal cotizacionObligatoria;

	@ApiModelProperty(value = "subtotal a pagar al fondo", position = 10)
	private BigDecimal subtotalFondo;

	@ApiModelProperty(value = "valor pagado por concepto de seguros", position = 11)
	private BigDecimal seguros;

	@ApiModelProperty(value = "comision fija a pagar", position = 12)
	private BigDecimal comision;
	
	@ApiModelProperty(value = "subtotal retenciones", position = 13)
	private BigDecimal subtotalRetenciones;
	
	@ApiModelProperty(value = "subtotal retenciones", position = 14)
	private BigDecimal tipoMovimiento;

	@ApiModelProperty(value = "fecha de movimiento del afiliado", position = 15)
	private String fechaMovimiento;

	@ApiModelProperty(value = "valor conciliado", position = 16)
	private BigDecimal valorConciliado;

	@ApiModelProperty(value = "numero de cuotas conciliadas", position = 17)
	private BigDecimal numeroCuotasConciliadas;

	@ApiModelProperty(value = "tipo de fondo", position = 18)
	private String tipoFondo;

	@ApiModelProperty(value = "usuario ultima modificacion", position = 19)
	private String usuario;

	@ApiModelProperty(value = "fecha de ultima modificacion", position = 20)
	private String fechaModificacion;

	@ApiModelProperty(value = "indicador de validacion", position = 21)
	private String indicadorUno;

	@ApiModelProperty(value = "indicador de uso futuro", position = 22)
	private String indicadorDos;

	@ApiModelProperty(value = "indicador de uso futuro", position = 23)
	private String indicadorTres;	
}
