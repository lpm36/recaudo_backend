package pe.com.integra.api.recaudo.application.dto.response;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "ResponseBusquedaCabeceraPlanillasDTO", description = "respuesta de obtencion de cabecera de planilla")
public class ResponseObtenerCabeceraPlanillasDTO extends ResponsePlanillaDTO{
	
	@ApiModelProperty(value = "remision medio magnetico", position = 5)
	private String medioMagnetico;
	
	@ApiModelProperty(value = "tipo de formulario", position = 6)
	private String tipoFormulario;

	@ApiModelProperty(value = "descripcion del tipo de planilla", position = 8)
	private String descTipoPlanilla;
	
	@ApiModelProperty(value = "nombre de la entidad recaudadora", position = 15)
	private String nombreEntidadRec;
	
	@ApiModelProperty(value = "descripcion del tipo de accion", position = 29)
	private String descTipoAccion;
	
	@ApiModelProperty(value = "nombre del empleador", position = 30)
	private String nombreEmpleador;
	
	@ApiModelProperty(value = "giro de negocio", position = 30)
	private String giroNegocio;
	
	@ApiModelProperty(value = "sucursal del empleador", position = 30)
	private String sucursalEmpleador;
	
	@ApiModelProperty(value = "direccion del empleador", position = 30)
	private String direccionEmpleador;
	
	@ApiModelProperty(value = "numero dpto/mz/km/int", position = 30)
	private String numeroDepartamento;
	
	@ApiModelProperty(value = "codigo postal empleador", position = 30)
	private String codigoPostalEmpleador;
	
	@ApiModelProperty(value = "distrito del empleador", position = 30)
	private String distritoEmpleador;
	
	@ApiModelProperty(value = "ciudad del empleador", position = 30)
	private String ciudadEmpleador;
	
	@ApiModelProperty(value = "fecha de modificacion", position = 30)
	private String fechaModificacion;
	
	@ApiModelProperty(value = "departamento del empleador", position = 30)
	private String departamentoEmpleador;
	
	@ApiModelProperty(value = "usuario de ultima modificacion", position = 30)
	private String usuarioModificacion;
	
	@ApiModelProperty(value = "urbanizacion del empleador", position = 30)
	private String urbanizacionEmpleador;
	
	@ApiModelProperty(value = "codigo ciudad del empleador", position = 30)
	private String codigoCiudadEmpleador;
	
	@ApiModelProperty(value = "telefono del empleador", position = 30)
	private String telefonoEmpleador;
	
	@ApiModelProperty(value = "ruc de empresa con la que se fusiono", position = 30)
	private String fusionado;
	
	@ApiModelProperty(value = "nombre del empleador", position = 30)
	private String nombreElaborador;
	
	@ApiModelProperty(value = "area de empresa elaborador", position = 30)
	private String areaEmpresa;
	
	@ApiModelProperty(value = "telefono de contacto", position = 30)
	private String telefonoContacto;
	
	@ApiModelProperty(value = "anexo de contacto", position = 30)
	private String anexoContacto;
	
	@ApiModelProperty(value = "numero de liquidacion de cobranza", position = 30)
	private String numeroLiquidacion;
	
	@ApiModelProperty(value = "preimpreso que regulariza", position = 30)
	private String regularizaPlanilla;
	
	@ApiModelProperty(value = "total obl. comple. afi", position = 30)
	private BigDecimal totalObligatorioAfiliado;
	
	@ApiModelProperty(value = "total obl. comple. emp", position = 30)
	private BigDecimal totalObligatorioEmpleador;
	
	@ApiModelProperty(value = "total aprt con fin prev.", position = 30)
	private BigDecimal totalCotConFinPrevisional;
	
	@ApiModelProperty(value = "total aprt sin fin prev.", position = 30)
	private BigDecimal totalCotSinFinPrevisional;
	
	@ApiModelProperty(value = "total aprt voluntaria", position = 30)
	private BigDecimal totalCotVoluntaria;
	
	@ApiModelProperty(value = "total aprt CTS", position = 30)
	private BigDecimal totalCts;
	
	@ApiModelProperty(value = "intereses fondo", position = 30)
	private BigDecimal interesesFondo;
	
	@ApiModelProperty(value = "descripcion estado de la planilla", position = 30)
	private String descEstadoPlanilla;
	
	@ApiModelProperty(value = "descripcion estado conciliacion", position = 30)
	private String descEstadoConciliacion;
	
	@ApiModelProperty(value = "descripcion tipo de pago planilla", position = 30)
	private String descTipoPagoPlanilla;	
	
	@ApiModelProperty(value = "Numero de Hojas Adicionales", position = 30)
	private BigDecimal numeroHojasAdicionales;
	
	@ApiModelProperty(value = "forma de pago al fondo", position = 30)
	private String formaPagoFondo;
	
	@ApiModelProperty(value = "descripcion forma de pago al fondo", position = 30)
	private String descFormaPagoFondo;
	
	@ApiModelProperty(value = "numero de cheque de pago", position = 30)
	private String numeroChequePago;
	
	@ApiModelProperty(value = "nombre del banco de pago", position = 30)
	private String bancoPago;
	
	@ApiModelProperty(value = "grupo de riesgo", position = 30)
	private String grupoRiesgo;
	
	@ApiModelProperty(value = "descripcion del tipo de rugro de subrogacion", position = 30)
	private String tipoAccion;
	
	@ApiModelProperty(value = "suma detalles", position = 30)
	private BigDecimal sumaDetalles;
	
	@ApiModelProperty(value = "int asume afp fd", position = 30)
	private BigDecimal intAsumeAfp;
	
	@ApiModelProperty(value = "observacion disponibilidad planilla fisica", position = 30)
	private String observacion;
	

	@ApiModelProperty(value = "total afiliados", position = 14)
	private BigDecimal totalAfiliados;
	
	@ApiModelProperty(value = "total remuneraciones", position = 15)
	private BigDecimal totalRemuneraciones;
	
	@ApiModelProperty(value = "total cotizacion obligatorio", position = 16)
	private BigDecimal totalCotizacionObligatorio;
	
	@ApiModelProperty(value = "subtotal fondo de pensiones", position = 17)
	private BigDecimal subtotalFondoPensiones;
	
	@ApiModelProperty(value = "penalidades fondo", position = 18)
	private BigDecimal penalidadesFondo;
	
	@ApiModelProperty(value = "total a pagar al fondo", position = 19)
	private BigDecimal totalPagarFondo;
	
	@ApiModelProperty(value = "suma de aportes informados", position = 20)
	private BigDecimal sumaAportesInformados;
	
	@ApiModelProperty(value = "suma aportes fondo calculados", position = 21)
	private BigDecimal sumaAportesFondoCalculados;
	
	@ApiModelProperty(value = "valor conciliado fondo", position = 22)
	private BigDecimal valorConciliadoFondo;
	
	@ApiModelProperty(value = "numero de cuotas conciliadas", position = 23)
	private BigDecimal nroCuotasConciliadas;
	
	@ApiModelProperty(value = "valor a pagar efectivo fond", position = 24)
	private BigDecimal valorPagarEfectivoFondo;
	
	@ApiModelProperty(value = "planilla a pagar", position = 25)
	private BigDecimal planillaAPagar;
	
	@ApiModelProperty(value = "indicardor forzar acreditacicon", position = 26)
	private String indicadorForzarAcreditacion;
	
	@ApiModelProperty(value = "indicador dos", position = 27)
	private String indicadorDos;
	
	@ApiModelProperty(value = "indicador tres", position = 28)
	private String indicadorTres;

}