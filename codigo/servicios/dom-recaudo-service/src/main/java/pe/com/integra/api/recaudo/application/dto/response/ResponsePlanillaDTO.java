package pe.com.integra.api.recaudo.application.dto.response;

import java.math.BigDecimal;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@ApiModel(value = "ResponsePlanillasDTO", description = "respuesta para una planilla")
public class ResponsePlanillaDTO {

	@ApiModelProperty(value = "lote de planilla", position = 1)
	private BigDecimal lote;
	
	@ApiModelProperty(value = "numero de planilla interno", position = 2)
	private BigDecimal planillaInterno;
	
	@ApiModelProperty(value = "numero de planilla fisico", position = 3)
	private BigDecimal planillaFisico;
	
	@ApiModelProperty(value = "numero de pre impreso", position = 4)
	private String nroPreimpreso;
	
	@ApiModelProperty(value = "tipo de planilla", position = 5)
	private String tipoPlanilla;
	
	@ApiModelProperty(value = "estado de planilla", position = 6)
	private String estadoPlanilla;

	@ApiModelProperty(value = "estado de conciliacion", position = 7)
	private String estadoConciliacion;
	
	@ApiModelProperty(value = "tipo de pago planilla", position = 8)
	private String tipoPagoPlanilla;
	
	@ApiModelProperty(value = "periodo de devengue", position = 9)
	private String periodoDevengue;
	
	@ApiModelProperty(value = "fecha de pago", position = 10)
	private String fechaPago;
	
	@ApiModelProperty(value = "codigo entidad recaudadora", position = 11)
	private String codigoEntidadRec;

	@ApiModelProperty(value = "tipo de identificacion", position = 12)
	private String tipoIdentificacion;
	
	@ApiModelProperty(value = "numero de identificacion", position = 13)
	private String numeroIdentificacion;

	@ApiModelProperty(value = "identificador de afp origen 'IN': integra y 'HO': horizonte'", position = 14)
	private String afp;
}
