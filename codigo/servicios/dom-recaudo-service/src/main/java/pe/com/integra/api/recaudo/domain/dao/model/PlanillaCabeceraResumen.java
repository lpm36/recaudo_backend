package pe.com.integra.api.recaudo.domain.dao.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PlanillaCabeceraResumen {

	private BigDecimal lote;
	private BigDecimal planillaInterno;
	private BigDecimal planillaFisico;
	private String nroPreimpreso;
	private String tipoPlanilla;
	private String estadoPlanilla;
	private String estadoConciliacion;
	private String tipoPagoPlanilla;
	private String periodoDevengue;
	private String fechaPago;
	private String codigoEntidadRec;
	private String tipoIdentificacion;
	private String numeroIdentificacion;
}
