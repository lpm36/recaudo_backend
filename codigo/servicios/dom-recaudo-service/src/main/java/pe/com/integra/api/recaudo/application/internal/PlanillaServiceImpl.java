package pe.com.integra.api.recaudo.application.internal;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import pe.com.integra.api.recaudo.application.PlanillaService;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDTO;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseObtenerCabeceraPlanillasDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseBusquedaPlanillasDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseBusquedaPlanillasDetalleDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseObtenerPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponsePlanillaDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponsePlanillaDetalleDTO;
import pe.com.integra.api.recaudo.domain.dao.PlanillaDao;
import pe.com.integra.api.recaudo.domain.dao.model.Planilla;
import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;
import pe.com.integra.api.recaudo.infraestructure.exception.NotDataException;
import pe.com.integra.api.recaudo.infraestructure.util.Constantes;
import pe.com.integra.api.recaudo.infraestructure.util.Utils;

@Slf4j
@Service
public class PlanillaServiceImpl implements PlanillaService {

	@Autowired
	PlanillaDao planillaDao;

	@SuppressWarnings("unchecked")
	@Override
	public ResponseBusquedaPlanillasDTO buscarPlanillas(RequestBusquedaPlanillaDTO request) {

		log.info("Ini buscarPlanillas: request {}", request.toString());

		ResponseBusquedaPlanillasDTO responseBusquedaPlanillasDTO = new ResponseBusquedaPlanillasDTO();
		List<ResponsePlanillaDTO> listaBusqueda = new ArrayList<>();

		setValuesBusquedaCabecera(request);

		Map<String, Object> resultado = planillaDao.buscarPlanillas(request);

		List<Planilla> planillas = (List<Planilla>) resultado.get(Constantes.MAPPER_PLANILLA_BUS);
		BigDecimal cantidad = (BigDecimal) resultado.get(Constantes.P_CANT);

		if (!planillas.isEmpty()) {
			for (Planilla planilla : planillas) {
				ResponsePlanillaDTO dtoPlanilla = new ResponsePlanillaDTO();
				dtoPlanilla.setLote(planilla.getLote());
				dtoPlanilla.setPlanillaInterno(planilla.getPlanillaInterno());
				dtoPlanilla.setPlanillaFisico(planilla.getPlanillaFisico());
				dtoPlanilla.setNroPreimpreso(planilla.getNroPreimpreso());
				dtoPlanilla.setTipoPlanilla(planilla.getTipoPlanilla());
				dtoPlanilla.setEstadoPlanilla(planilla.getEstadoPlanilla());
				dtoPlanilla.setEstadoConciliacion(planilla.getEstadoConciliacion());
				dtoPlanilla.setTipoPagoPlanilla(planilla.getTipoPagoPlanilla());
				dtoPlanilla.setPeriodoDevengue(planilla.getPeriodoDevengue());
				dtoPlanilla.setFechaPago(planilla.getFechaPago());
				dtoPlanilla.setCodigoEntidadRec(planilla.getCodigoEntidadRec());
				dtoPlanilla.setTipoIdentificacion(planilla.getTipoIdentificacion());
				dtoPlanilla.setNumeroIdentificacion(planilla.getNumeroIdentificacion());
				dtoPlanilla.setAfp(planilla.getAfp());
				listaBusqueda.add(dtoPlanilla);
			}
		}

		responseBusquedaPlanillasDTO.setCantidadTotalEncontrados(cantidad);
		responseBusquedaPlanillasDTO.setPlanillaCabeceras(listaBusqueda);

		log.info("Ini buscarPlanillas: request {}", request.toString());

		return responseBusquedaPlanillasDTO;
	}

	private RequestBusquedaPlanillaDTO setValuesBusquedaCabecera(RequestBusquedaPlanillaDTO request) {
		request.setLote(Utils.validBigDecimalZero(request.getLote()));
		request.setPlanillaInterno(Utils.validBigDecimalZero(request.getPlanillaInterno()));
		request.setNroPreimpreso(Utils.validString(request.getNroPreimpreso()));	
		request.setTipoPlanilla(Utils.validString(request.getTipoPlanilla()));
		request.setEstadoPlanilla(Utils.validString(request.getEstadoPlanilla()));
		request.setEstadoConciliacion(Utils.validString(request.getEstadoConciliacion()));
		request.setPeriodoDevengue(Utils.validString(request.getPeriodoDevengue()));
		request.setTipoIdentificacion(Utils.validString(request.getTipoIdentificacion()));
		request.setNumeroIdentificacion(Utils.validString(request.getNumeroIdentificacion()));
		request.setFechaPagoInicial(Utils.validString(request.getFechaPagoInicial()));
		request.setFechaPagoFinal(Utils.validString(request.getFechaPagoFinal()));
		request.setRegistroInicial(Utils.validIntegerOne(request.getRegistroInicial()));
		request.setRegistroFinal(Utils.validIntegerOne(request.getRegistroFinal()));

		return request;
	}

	private RequestBusquedaPlanillaDetalleDTO setValuesBusquedaDetalle(RequestBusquedaPlanillaDetalleDTO request) {

		request.setLote(Utils.validBigDecimalZero(request.getLote()));
		request.setPlanillaInterno(Utils.validBigDecimalZero(request.getPlanillaInterno()));
		request.setPeriodoDevengue(Utils.validString(request.getPeriodoDevengue()));
		request.setCuspp(Utils.validString(request.getCuspp()));
		request.setTipoIdentificacion(Utils.validString(request.getTipoIdentificacion()));
		request.setNumeroIdentificacion(Utils.validString(request.getNumeroIdentificacion()));
		request.setRegistroInicial(Utils.validIntegerOne(request.getRegistroInicial()));
		request.setRegistroFinal(Utils.validIntegerOne(request.getRegistroFinal()));

		return request;
	}

	@Override
	public ResponseObtenerCabeceraPlanillasDTO obtenerCabeceraPlanilla(String usuario, BigDecimal lotePlanilla,
			BigDecimal numeroPlanilla) {

		log.info("Ini obtenerCabeceraPlanilla: numeroPlanilla {}", numeroPlanilla);

		ResponseObtenerCabeceraPlanillasDTO dtoCabeceraPlanilla = new ResponseObtenerCabeceraPlanillasDTO();
		List<Planilla> listaPlanilla = planillaDao.obtenerPlanilla(lotePlanilla, numeroPlanilla);

		if (listaPlanilla.isEmpty()) {
			throw new NotDataException(Constantes.MESSAGE_ERROR);
		}

		Planilla planilla = listaPlanilla.get(0);

		dtoCabeceraPlanilla.setLote(planilla.getLote());
		dtoCabeceraPlanilla.setPlanillaFisico(planilla.getPlanillaFisico());
		dtoCabeceraPlanilla.setPlanillaInterno(planilla.getPlanillaInterno());
		dtoCabeceraPlanilla.setNroPreimpreso(planilla.getNroPreimpreso());
		dtoCabeceraPlanilla.setMedioMagnetico(planilla.getMedioMagnetico());
		dtoCabeceraPlanilla.setTipoFormulario(planilla.getTipoFormulario());
		dtoCabeceraPlanilla.setTipoPlanilla(planilla.getTipoPlanilla());
		dtoCabeceraPlanilla.setDescTipoPlanilla(planilla.getDescTipoPlanilla());
		dtoCabeceraPlanilla.setEstadoPlanilla(planilla.getEstadoPlanilla());
		dtoCabeceraPlanilla.setEstadoConciliacion(planilla.getEstadoConciliacion());
		dtoCabeceraPlanilla.setTipoPagoPlanilla(planilla.getTipoPagoPlanilla());
		dtoCabeceraPlanilla.setPeriodoDevengue(planilla.getPeriodoDevengue());
		dtoCabeceraPlanilla.setFechaPago(planilla.getFechaPago());
		dtoCabeceraPlanilla.setCodigoEntidadRec(planilla.getCodigoEntidadRec());
		dtoCabeceraPlanilla.setNombreEntidadRec(planilla.getNombreEntidadRec());
		dtoCabeceraPlanilla.setTipoIdentificacion(planilla.getTipoIdentificacion());
		dtoCabeceraPlanilla.setNumeroIdentificacion(planilla.getNumeroIdentificacion());
		dtoCabeceraPlanilla.setTotalAfiliados(planilla.getTotalAfiliados());
		dtoCabeceraPlanilla.setTotalRemuneraciones(planilla.getTotalRemuneraciones());
		dtoCabeceraPlanilla.setTotalCotizacionObligatorio(planilla.getTotalCotizacionObligatorio());
		dtoCabeceraPlanilla.setSubtotalFondoPensiones(planilla.getSubtotalFondoPensiones());
		dtoCabeceraPlanilla.setPenalidadesFondo(planilla.getPenalidadesFondo());
		dtoCabeceraPlanilla.setTotalPagarFondo(planilla.getTotalPagarFondo());
		dtoCabeceraPlanilla.setSumaAportesInformados(planilla.getSumaAportesInformados());
		dtoCabeceraPlanilla.setSumaAportesFondoCalculados(planilla.getSumaAportesFondoCalculados());
		dtoCabeceraPlanilla.setValorConciliadoFondo(planilla.getValorConciliadoFondo());
		dtoCabeceraPlanilla.setNroCuotasConciliadas(planilla.getNroCuotasConciliadas());
		dtoCabeceraPlanilla.setValorPagarEfectivoFondo(planilla.getValorPagarEfectivoFondo());
		dtoCabeceraPlanilla.setPlanillaAPagar(planilla.getPlanillaAPagar());
		dtoCabeceraPlanilla.setIndicadorForzarAcreditacion(planilla.getIndicadorForzarAcreditacion());
		dtoCabeceraPlanilla.setIndicadorDos(planilla.getIndicadorDos());
		dtoCabeceraPlanilla.setIndicadorTres(planilla.getIndicadorTres());
		dtoCabeceraPlanilla.setDescTipoAccion(planilla.getDescTipoAccion());
		dtoCabeceraPlanilla.setNombreEmpleador(planilla.getNombreEmpleador());
		dtoCabeceraPlanilla.setGiroNegocio(planilla.getGiroNegocio());
		dtoCabeceraPlanilla.setSucursalEmpleador(planilla.getSucursalEmpleador());
		dtoCabeceraPlanilla.setDireccionEmpleador(planilla.getDireccionEmpleador());
		dtoCabeceraPlanilla.setNumeroDepartamento(planilla.getNumeroDepartamento());
		dtoCabeceraPlanilla.setCodigoPostalEmpleador(planilla.getCodigoPostalEmpleador());
		dtoCabeceraPlanilla.setDistritoEmpleador(planilla.getDistritoEmpleador());
		dtoCabeceraPlanilla.setCiudadEmpleador(planilla.getCiudadEmpleador());
		dtoCabeceraPlanilla.setFechaModificacion(planilla.getFechaModificacion());
		dtoCabeceraPlanilla.setDepartamentoEmpleador(planilla.getDepartamentoEmpleador());
		dtoCabeceraPlanilla.setUsuarioModificacion(planilla.getUsuarioModificacion());
		dtoCabeceraPlanilla.setUrbanizacionEmpleador(planilla.getUrbanizacionEmpleador());
		dtoCabeceraPlanilla.setCodigoCiudadEmpleador(planilla.getCodigoCiudadEmpleador());
		dtoCabeceraPlanilla.setTelefonoEmpleador(planilla.getTelefonoEmpleador());
		dtoCabeceraPlanilla.setFusionado(planilla.getFusionado());
		dtoCabeceraPlanilla.setNombreElaborador(planilla.getNombreElaborador());
		dtoCabeceraPlanilla.setAreaEmpresa(planilla.getAreaEmpresa());
		dtoCabeceraPlanilla.setTelefonoContacto(planilla.getTelefonoContacto());
		dtoCabeceraPlanilla.setAnexoContacto(planilla.getAnexoContacto());
		dtoCabeceraPlanilla.setNumeroLiquidacion(planilla.getNumeroLiquidacion());
		dtoCabeceraPlanilla.setRegularizaPlanilla(planilla.getRegularizaPlanilla());
		dtoCabeceraPlanilla.setTotalObligatorioAfiliado(planilla.getTotalObligatorioAfiliado());
		dtoCabeceraPlanilla.setTotalObligatorioEmpleador(planilla.getTotalObligatorioEmpleador());
		dtoCabeceraPlanilla.setTotalCotConFinPrevisional(planilla.getTotalCotConFinPrevisional());
		dtoCabeceraPlanilla.setTotalCotSinFinPrevisional(planilla.getTotalCotSinFinPrevisional());
		dtoCabeceraPlanilla.setTotalCotVoluntaria(planilla.getTotalCotVoluntaria());
		dtoCabeceraPlanilla.setTotalCts(planilla.getTotalCts());
		dtoCabeceraPlanilla.setInteresesFondo(planilla.getInteresesFondo());
		dtoCabeceraPlanilla.setDescEstadoPlanilla(planilla.getDescEstadoPlanilla());
		dtoCabeceraPlanilla.setDescEstadoConciliacion(planilla.getDescEstadoConciliacion());
		dtoCabeceraPlanilla.setDescTipoPagoPlanilla(planilla.getDescTipoPagoPlanilla());
		dtoCabeceraPlanilla.setNumeroHojasAdicionales(planilla.getNumeroHojasAdicionales());
		dtoCabeceraPlanilla.setFormaPagoFondo(planilla.getFormaPagoFondo());
		dtoCabeceraPlanilla.setDescFormaPagoFondo(planilla.getDescFormaPagoFondo());
		dtoCabeceraPlanilla.setNumeroChequePago(planilla.getNumeroChequePago());
		dtoCabeceraPlanilla.setBancoPago(planilla.getBancoPago());
		dtoCabeceraPlanilla.setGrupoRiesgo(planilla.getGrupoRiesgo());
		dtoCabeceraPlanilla.setTipoAccion(planilla.getTipoAccion());
		dtoCabeceraPlanilla.setSumaDetalles(planilla.getSumaDetalles());
		dtoCabeceraPlanilla.setIntAsumeAfp(planilla.getIntAsumeAfp());
		dtoCabeceraPlanilla.setObservacion(planilla.getObservacion());
		dtoCabeceraPlanilla.setAfp(planilla.getAfp());

		log.info("Fin obtenerCabeceraPlanilla: numeroPlanilla {}", numeroPlanilla);

		return dtoCabeceraPlanilla;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseBusquedaPlanillasDetalleDTO buscarPlanillasDetalles(RequestBusquedaPlanillaDetalleDTO request) {

		log.info("Ini buscarPlanillasDetalles: request {}", request.toString());

		ResponseBusquedaPlanillasDetalleDTO dtoPlanillasDetalle = new ResponseBusquedaPlanillasDetalleDTO();
		List<ResponsePlanillaDetalleDTO> listaDtoDetalle = new ArrayList<>();

		setValuesBusquedaDetalle(request);

		Map<String, Object> resultado = planillaDao.buscarPlanillasDetalles(request);

		List<PlanillaDetalle> listaDetalles = (List<PlanillaDetalle>) resultado.get(Constantes.MAPPER_PLANILLA_DET);
		BigDecimal cantidad = (BigDecimal) resultado.get(Constantes.P_CANT);

		if (!listaDetalles.isEmpty()) {
			for (PlanillaDetalle detalle : listaDetalles) {

				ResponsePlanillaDetalleDTO dtoDetalle = new ResponsePlanillaDetalleDTO();

				dtoDetalle.setLote(detalle.getLote());
				dtoDetalle.setPlanillaFisico(detalle.getPlanillaFisico());
				dtoDetalle.setPlanillaInterno(detalle.getPlanillaInterno());
				dtoDetalle.setNroPreimpreso(detalle.getNroPreimpreso());
				dtoDetalle.setTipoPlanilla(detalle.getTipoPlanilla());
				dtoDetalle.setEstadoPlanilla(detalle.getEstadoPlanilla());
				dtoDetalle.setEstadoConciliacion(detalle.getEstadoConciliacion());
				dtoDetalle.setTipoPagoPlanilla(detalle.getTipoPagoPlanilla());
				dtoDetalle.setPeriodoDevengue(detalle.getPeriodoDevengue());
				dtoDetalle.setFechaPago(detalle.getFechaPago());
				dtoDetalle.setCodigoEntidadRec(detalle.getCodigoEntidadRec());
				dtoDetalle.setTipoIdentificacion(detalle.getTipoIdentificacion());
				dtoDetalle.setNumeroIdentificacion(detalle.getNumeroIdentificacion());

				dtoDetalle.setPagina(detalle.getPagina());
				dtoDetalle.setSecuencia(detalle.getSecuencia());
				dtoDetalle.setCuspp(detalle.getCuspp());
				dtoDetalle.setPrimerApellido(detalle.getPrimerApellido());
				dtoDetalle.setSegundoApellido(detalle.getSegundoApellido());
				dtoDetalle.setPrimerNombre(detalle.getPrimerNombre());
				dtoDetalle.setSegundoNombre(detalle.getSegundoNombre());
				dtoDetalle.setRemuneracionAsegurable(detalle.getRemuneracionAsegurable());
				dtoDetalle.setCotizacionObligatoria(detalle.getCotizacionObligatoria());
				dtoDetalle.setSubtotalFondo(detalle.getSubtotalFondo());
				dtoDetalle.setSeguros(detalle.getSeguros());
				dtoDetalle.setComision(detalle.getComision());
				dtoDetalle.setSubtotalRetenciones(detalle.getSubtotalRetenciones());
				dtoDetalle.setTipoMovimiento(detalle.getTipoMovimiento());
				dtoDetalle.setFechaMovimiento(detalle.getFechaMovimiento());
				dtoDetalle.setValorConciliado(detalle.getValorConciliado());
				dtoDetalle.setNumeroCuotasConciliadas(detalle.getNumeroCuotasConciliadas());
				dtoDetalle.setTipoFondo(detalle.getTipoFondo());
				dtoDetalle.setUsuario(detalle.getUsuario());
				dtoDetalle.setFechaModificacion(detalle.getFechaModificacion());
				dtoDetalle.setIndicadorUno(detalle.getIndicadorUno());
				dtoDetalle.setIndicadorDos(detalle.getIndicadorDos());
				dtoDetalle.setIndicadorTres(detalle.getIndicadorTres());
				dtoDetalle.setAfp(detalle.getAfpDetalle());

				listaDtoDetalle.add(dtoDetalle);
			}
		}

		dtoPlanillasDetalle.setCantidadTotalEncontrados(cantidad);
		dtoPlanillasDetalle.setPlanillaDetalles(listaDtoDetalle);

		log.info("Fin buscarPlanillasDetalles: request {}", request.toString());

		return dtoPlanillasDetalle;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ResponseObtenerPlanillaDetalleDTO obtenerDetallePlanilla(PlanillaDetalle parametrosDetalle) {

		log.info("Fin buscarPlanillasDetalles: parametrosDetalle {}", parametrosDetalle.toString());

		ResponseObtenerPlanillaDetalleDTO dtoPlanillaDetalle = new ResponseObtenerPlanillaDetalleDTO();

		Map<String, Object> resultado = planillaDao.obtenerDetallePlanilla(parametrosDetalle);

		List<Planilla> listaPlanilla = (List<Planilla>) resultado.get(Constantes.MAPPER_PLANILLA_BUS);
		List<PlanillaDetalle> listaPlanillaDetalle = (List<PlanillaDetalle>) resultado
				.get(Constantes.MAPPER_OBTENER_PLANILLA_DETALLE);

		if (listaPlanilla.isEmpty() || listaPlanillaDetalle.isEmpty()) {
			throw new NotDataException(Constantes.MESSAGE_ERROR);
		}

		Planilla planilla = listaPlanilla.get(0);
		PlanillaDetalle planillaDetalle = listaPlanillaDetalle.get(0);
		
		dtoPlanillaDetalle.setLote(planilla.getLote());
		dtoPlanillaDetalle.setPlanillaInterno(planilla.getPlanillaInterno());
		dtoPlanillaDetalle.setPlanillaFisico(planilla.getPlanillaFisico());
		dtoPlanillaDetalle.setNroPreimpreso(planilla.getNroPreimpreso());
		dtoPlanillaDetalle.setTipoPlanilla(planilla.getTipoPlanilla());
		dtoPlanillaDetalle.setEstadoPlanilla(planilla.getEstadoPlanilla());
		dtoPlanillaDetalle.setEstadoConciliacion(planilla.getEstadoConciliacion());
		dtoPlanillaDetalle.setTipoPagoPlanilla(planilla.getTipoPagoPlanilla());
		dtoPlanillaDetalle.setPeriodoDevengue(planilla.getPeriodoDevengue());
		dtoPlanillaDetalle.setFechaPago(planilla.getFechaPago());
		dtoPlanillaDetalle.setCodigoEntidadRec(planilla.getCodigoEntidadRec());
		dtoPlanillaDetalle.setTipoIdentificacion(planilla.getTipoIdentificacion());
		dtoPlanillaDetalle.setNumeroIdentificacion(planilla.getNumeroIdentificacion());

		dtoPlanillaDetalle.setPagina(planillaDetalle.getPagina());
		dtoPlanillaDetalle.setSecuencia(planillaDetalle.getSecuencia());
		dtoPlanillaDetalle.setCuspp(planillaDetalle.getCuspp());
		dtoPlanillaDetalle.setPrimerApellido(planillaDetalle.getPrimerApellido());
		dtoPlanillaDetalle.setSegundoApellido(planillaDetalle.getSegundoApellido());
		dtoPlanillaDetalle.setPrimerNombre(planillaDetalle.getPrimerNombre());
		dtoPlanillaDetalle.setSegundoNombre(planillaDetalle.getSegundoNombre());
		dtoPlanillaDetalle.setRemuneracionAsegurable(planillaDetalle.getRemuneracionAsegurable());
		dtoPlanillaDetalle.setCotizacionObligatoria(planillaDetalle.getCotizacionObligatoria());
		dtoPlanillaDetalle.setCotizacionCompleAfi(planillaDetalle.getCotizacionCompleAfi());
		dtoPlanillaDetalle.setCotizacionCompleEmp(planillaDetalle.getCotizacionCompleEmp());
		dtoPlanillaDetalle.setCotizacionConFinPre(planillaDetalle.getCotizacionConFinPre());
		dtoPlanillaDetalle.setCotizacionSinFinPre(planillaDetalle.getCotizacionSinFinPre());
		dtoPlanillaDetalle.setCotizacionVolEmp(planillaDetalle.getCotizacionVolEmp());
		dtoPlanillaDetalle.setCotizacionCTS(planillaDetalle.getCotizacionCTS());
		dtoPlanillaDetalle.setSubtotalFondo(planillaDetalle.getSubtotalFondo());
		dtoPlanillaDetalle.setContribucionIPSS(planillaDetalle.getContribucionIPSS());
		dtoPlanillaDetalle.setSeguros(planillaDetalle.getSeguros());
		dtoPlanillaDetalle.setComision(planillaDetalle.getComision());
		dtoPlanillaDetalle.setComisionPorcentaje(planillaDetalle.getComisionPorcentaje());
		dtoPlanillaDetalle.setSubtotalRetenciones(planillaDetalle.getSubtotalRetenciones());
		dtoPlanillaDetalle.setTotalDetalle(planillaDetalle.getTotalDetalle());
		dtoPlanillaDetalle.setTipoMovimiento(planillaDetalle.getTipoMovimiento());
		dtoPlanillaDetalle.setFechaMovimiento(planillaDetalle.getFechaMovimiento());
		dtoPlanillaDetalle.setFechaFinalizacion(planillaDetalle.getFechaFinalizacion());
		dtoPlanillaDetalle.setTipoIdentificacionSubsi(planillaDetalle.getTipoIdentificacionSubsi());
		dtoPlanillaDetalle.setIdentificacionSubsi(planillaDetalle.getIdentificacionSubsi());
		dtoPlanillaDetalle.setValorConciliado(planillaDetalle.getValorConciliado());
		dtoPlanillaDetalle.setNumeroCuotasConciliadas(planillaDetalle.getNumeroCuotasConciliadas());
		dtoPlanillaDetalle.setTipoFondo(planillaDetalle.getTipoFondo());
		dtoPlanillaDetalle.setUsuario(planillaDetalle.getUsuario());
		dtoPlanillaDetalle.setFechaModificacion(planillaDetalle.getFechaModificacion());
		dtoPlanillaDetalle.setIndicadorUno(planillaDetalle.getIndicadorUno());
		dtoPlanillaDetalle.setIndicadorDos(planillaDetalle.getIndicadorDos());
		dtoPlanillaDetalle.setIndicadorTres(planillaDetalle.getIndicadorTres());
		dtoPlanillaDetalle.setDevengue(planillaDetalle.getDevengue());

		dtoPlanillaDetalle.setSecuenciaCuota(planillaDetalle.getSecuenciaCuota());
		dtoPlanillaDetalle.setInteresTardioFdo(planillaDetalle.getInteresTardioFdo());
		dtoPlanillaDetalle.setInteresTardioSeg(planillaDetalle.getInteresTardioSeg());
		dtoPlanillaDetalle.setInteresTardioCom(planillaDetalle.getInteresTardioCom());
		dtoPlanillaDetalle.setInteresFracFondo(planillaDetalle.getInteresFracFondo());
		dtoPlanillaDetalle.setInteresFracSeg(planillaDetalle.getInteresFracSeg());
		dtoPlanillaDetalle.setInteresFracCom(planillaDetalle.getInteresFracCom());
		dtoPlanillaDetalle.setRentabilidadFondo(planillaDetalle.getRentabilidadFondo());
		dtoPlanillaDetalle.setRentabilidadSeguro(planillaDetalle.getRentabilidadSeguro());
		dtoPlanillaDetalle.setRentabilidadComis(planillaDetalle.getRentabilidadComis());
		dtoPlanillaDetalle.setFinCuota(planillaDetalle.getFinCuota());
		dtoPlanillaDetalle.setIndicadorUnoRep(planillaDetalle.getIndicadorUnoRep());
		dtoPlanillaDetalle.setIndicadorDosRep(planillaDetalle.getIndicadorDosRep());
		dtoPlanillaDetalle.setFechaIngresoRep(planillaDetalle.getFechaIngresoRep());
		dtoPlanillaDetalle.setUsuarioIngresoRep(planillaDetalle.getUsuarioIngresoRep());
		dtoPlanillaDetalle.setFechaModificacionRep(planillaDetalle.getFechaModificacionRep());
		dtoPlanillaDetalle.setUsuarioModificacionRep(planillaDetalle.getUsuarioModificacionRep());

		dtoPlanillaDetalle.setAfp(planillaDetalle.getAfpDetalle());
		
		log.info("Fin buscarPlanillasDetalles: parametrosDetalle {}", parametrosDetalle.toString());

		return dtoPlanillaDetalle;
	}
}