package pe.com.integra.api.recaudo.infraestructure.util.annotation;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

public class ValidadorFecha implements ConstraintValidator<FechaValida, String> {

	private String formato;

	@Override
	public void initialize(FechaValida constraintAnnotation) {
		this.formato = constraintAnnotation.formato();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {

		if (value == null || value.equalsIgnoreCase(StringUtils.EMPTY))
			return true;

		SimpleDateFormat format = new SimpleDateFormat(formato);
		format.setLenient(false);

		try {
			format.parse(value.trim());
		} catch (ParseException pe) {
			return false;
		}
		return true;
	}
}