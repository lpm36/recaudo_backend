package pe.com.integra.api.recaudo.domain.dao.model;

import lombok.Data;

@Data
public class SecretAWS {

	private String driverClassName; 
	private String urlDataBase;
	private String usuario;
	private String clave;
	
	private String schema;
	
	private String driverClassName2; 
	private String urlDataBase2;
	private String usuario2;
	private String clave2;
	
	
}
