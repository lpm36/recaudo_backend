package pe.com.integra.api.recaudo.domain.dao.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Planilla extends PlanillaCabeceraResumen {
	
	private String medioMagnetico;
	private String tipoFormulario;
	private String descTipoPlanilla;
	private String nombreEntidadRec;
	private BigDecimal totalAfiliados;
	private BigDecimal totalRemuneraciones;
	private BigDecimal totalCotizacionObligatorio;
	private BigDecimal subtotalFondoPensiones;
	private BigDecimal penalidadesFondo;
	private BigDecimal totalPagarFondo;
	private BigDecimal sumaAportesInformados;
	private BigDecimal sumaAportesFondoCalculados;
	private BigDecimal valorConciliadoFondo;
	private BigDecimal nroCuotasConciliadas;
	private BigDecimal valorPagarEfectivoFondo;
	private BigDecimal planillaAPagar;
	private String indicadorForzarAcreditacion;
	private String indicadorDos;
	private String indicadorTres;
	private String descTipoAccion;
	private String nombreEmpleador;
	private String giroNegocio;
	private String sucursalEmpleador;
	private String direccionEmpleador;
	private String numeroDepartamento;
	private String codigoPostalEmpleador;
	private String distritoEmpleador;
	private String ciudadEmpleador;
	private String fechaModificacion;
	private String departamentoEmpleador;
	private String usuarioModificacion;
	private String urbanizacionEmpleador;
	private String codigoCiudadEmpleador;
	private String telefonoEmpleador;
	private String fusionado;
	private String nombreElaborador;
	private String areaEmpresa;
	private String telefonoContacto;
	private String anexoContacto;
	private String numeroLiquidacion;
	private String regularizaPlanilla;
	private BigDecimal totalObligatorioAfiliado;
	private BigDecimal totalObligatorioEmpleador;
	private BigDecimal totalCotConFinPrevisional;
	private BigDecimal totalCotSinFinPrevisional;
	private BigDecimal totalCotVoluntaria;
	private BigDecimal totalCts;
	private BigDecimal interesesFondo;
	private String descEstadoPlanilla;
	private String descEstadoConciliacion;
	private String descTipoPagoPlanilla;
	private BigDecimal numeroHojasAdicionales;
	private String formaPagoFondo;
	private String descFormaPagoFondo;
	private String numeroChequePago;
	private String bancoPago;
	private String grupoRiesgo;
	private String tipoAccion;
	private BigDecimal sumaDetalles;
	private BigDecimal intAsumeAfp;
	private String observacion;
	private String afp;
}
