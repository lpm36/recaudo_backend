package pe.com.integra.api.recaudo.infraestructure.exception;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import pe.com.integra.api.recaudo.application.dto.response.ResponseDTO;
import pe.com.integra.api.recaudo.infraestructure.util.Constantes;

@ControllerAdvice
public class ExceptionController {

	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllRunExceptions(Exception ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());

		StringWriter errors = new StringWriter();
		ex.printStackTrace(new PrintWriter(errors));

		details.add(errors.toString());

		ResponseDTO<List<String>> responseDTO = new ResponseDTO<>();
		responseDTO.setCode(Constantes.CODE_RESPONSE_ERROR);
		responseDTO.setStatus(false);
		responseDTO.setMessage(Constantes.MESSAGE_ERROR);
		responseDTO.setBody(details);

		return new ResponseEntity<>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public final ResponseEntity<Object> argumentosNoValidos(MethodArgumentNotValidException ex, WebRequest request) {
		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach(error -> {
			String fieldName;
			try {
				fieldName = ((FieldError) error).getField();
			} catch (Exception e) {
				fieldName = "crossFieldsValidation" + error.hashCode();
			}
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});

		ResponseDTO<Map<String, String>> responseDTO = new ResponseDTO<>();
		responseDTO.setCode(Constantes.CODE_RESPONSE_FIELDS_VALIDATION);
		responseDTO.setStatus(false);
		responseDTO.setMessage(Constantes.MESSAGE_FIELDS_VALIDATION);
		responseDTO.setBody(errors);

		return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> argumentosNoValidos(ConstraintViolationException ex, WebRequest request) {

		ResponseDTO<String> responseDTO = new ResponseDTO<>();
		responseDTO.setCode(Constantes.CODE_RESPONSE_FIELDS_VALIDATION);
		responseDTO.setStatus(false);
		responseDTO.setMessage(Constantes.MESSAGE_FIELDS_VALIDATION);
		responseDTO.setBody(ex.getMessage());

		return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	public final ResponseEntity<Object> argumentosTiposIncorrectos(MethodArgumentTypeMismatchException ex,
			WebRequest request) {

		ResponseDTO<String> responseDTO = new ResponseDTO<>();
		responseDTO.setCode(Constantes.CODE_RESPONSE_FIELDS_VALIDATION);
		responseDTO.setStatus(false);
		responseDTO.setMessage(Constantes.MESSAGE_FIELDS_VALIDATION);
		responseDTO.setBody(ex.getMessage());

		return new ResponseEntity<>(responseDTO, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(NotDataException.class)
	public final ResponseEntity<Object> handleMethod(NotDataException ex, WebRequest request) {
		ResponseDTO<String> responseDTO = new ResponseDTO<>();
		responseDTO.setCode(Constantes.CODE_RESPONSE_NOT_DATA);
		responseDTO.setStatus(true);
		responseDTO.setMessage(Constantes.MSG_RESPONSE_NOT_DATA);
		responseDTO.setBody(ex.getMessage());

		return new ResponseEntity<>(responseDTO, HttpStatus.OK);
	}
}