package pe.com.integra.api.recaudo.infraestructure.util;

import java.math.BigDecimal;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import pe.com.integra.api.recaudo.application.dto.response.ResponseDTO;

public class Utils {

	private Utils() {
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ResponseDTO returnResponseSuccessful(Object response) {
		ResponseDTO responseDTO = new ResponseDTO();
		responseDTO.setCode(10000);
		responseDTO.setStatus(true);
		responseDTO.setMessage(Constantes.MESSAGE_SUCCESSFUL);
		responseDTO.setBody(response);
		return responseDTO;
	}

	public static String validString(String value) {
		return Objects.toString(value, StringUtils.EMPTY).trim();
	}

	public static Integer validIntegerOne(Integer value) {
		return value != null ? value : Constantes.VALUE_ONE;
	}

	public static BigDecimal validBigDecimalZero(BigDecimal value) {
		return value != null ? value : BigDecimal.valueOf(Constantes.VALUE_ZERO);
	}
}