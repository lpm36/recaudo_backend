package pe.com.integra.api.recaudo.infraestructure.util.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ElementType.TYPE, ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = { ValidadorBusquedaPlanillaDetalle.class })
public @interface BusquedaPlanillaDetalleValida {
    String message() default "BusquedaPlanillaDetalleValida incorrecta";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
