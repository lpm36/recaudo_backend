package pe.com.integra.api.recaudo.infraestructure.exception;

public class NotDataException extends RuntimeException{

	private static final long serialVersionUID = 11116133198198117L;
	
	public NotDataException(String message) {
		super(message);
	}
}
