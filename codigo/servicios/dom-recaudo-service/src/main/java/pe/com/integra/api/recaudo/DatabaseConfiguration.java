package pe.com.integra.api.recaudo;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import lombok.extern.slf4j.Slf4j;
import pe.com.integra.api.recaudo.domain.dao.model.SecretAWS;

@Configuration
@Slf4j
public class DatabaseConfiguration {

	public static final String DRIVER_AS400 = "com.ibm.as400.access.AS400JDBCDriver";
	public static final String JDBC_URL_AS400 = "jdbc:as400://APPN.INTEGRA1;extended dynamic=true;package cache=false;package library=MINTDAT,MINTPRO;errors=basic;libraries=MINTDAT,MINTPRO";
	public static final String USER_AS400 = "SD0100";
	public static final String PW_AS400 = "EJNFSHT1";
	public static final String JNDI = "java:/dsRecaAs400";

	@Primary
	@Bean(name = "as400JdbcTemplate")
	public JdbcTemplate as400JdbcTemplate(@Qualifier("dataSourceJNDIAs400") DataSource dataSourceJNDIAs400) {
		return new JdbcTemplate(dataSourceJNDIAs400);
	}

	@Bean(name = "dataSourceJNDIAs400")
	public DataSource getJNDIConectionAs400(@Qualifier("secretsAWS") SecretAWS secretAWS) {

		DataSource dataSource = null;
		try {
			Context initialContext = new InitialContext();

			dataSource = (DataSource) initialContext.lookup(DatabaseConfiguration.JNDI);
			if (dataSource != null) {
				log.info("Conecto al JNDI al datasource OnPremice");
			} else {
				dataSource = dataSourceHikariAs400(secretAWS);
				log.info("Conecto Hikari as400");
			}
		} catch (Exception e1) {
			log.error("Error al crear Invocar JNDI as400" + e1.getStackTrace());
			try {
				dataSource = dataSourceHikariAs400(secretAWS);
				log.info("salio del hikari");
			} catch (Exception e2) {
				log.error("Error al crear JNDI Hikari as400:" + e2);
			}

		}
		return dataSource;
	}

	public DataSource dataSourceHikariAs400(SecretAWS secretAWS) {
		HikariConfig hikariConfig = new HikariConfig();
		hikariConfig.setDriverClassName(secretAWS.getDriverClassName());
		hikariConfig.setJdbcUrl(secretAWS.getUrlDataBase());
		hikariConfig.setUsername(secretAWS.getUsuario());
		hikariConfig.setPassword(secretAWS.getClave());
		hikariConfig.setMaximumPoolSize(5);
		hikariConfig.setMinimumIdle(2);
		hikariConfig.setConnectionTestQuery("SELECT 1 FROM sysibm.sysdummy1");
		log.info("****** finalizo config Hikari as400******");

		return new HikariDataSource(hikariConfig);
	}
	
	@Bean(name = "secretsAWS")
	public SecretAWS getSecretAWS() {
		
		//simula conexion AWS y obtiene los valores
		
		SecretAWS object =  new SecretAWS();
		object.setDriverClassName(DatabaseConfiguration.DRIVER_AS400);
		object.setUrlDataBase(DatabaseConfiguration.JDBC_URL_AS400);
		object.setUsuario(DatabaseConfiguration.USER_AS400);
		object.setClave (DatabaseConfiguration.PW_AS400);
		
		return object;
	}
	
}
