package pe.com.integra.api.recaudo.infraestructure.util;

public class Constantes {

	private Constantes() {
	}

	/* STOREES PROCEDURES */
	public static final String SP_BUSCARPLANILLA = "RECA0001_H";
	public static final String SP_OBTENERPLANILLA = "RECA0002_H";
	public static final String SP_BUSCARDETALLE = "RECA0003_H";
	public static final String SP_OBTENERDETALLE = "RECA0004_H";
	public static final String SCHEMA_MINTPRO = "MINTPRO";

	/* CURSORES AS400 */
	public static final String MAPPER_PLANILLA_CAB = "CURSCAB";
	public static final String MAPPER_PLANILLA_BUS = "CURSBUS";
	public static final String MAPPER_PLANILLA_DET = "CURSDET";
	public static final String MAPPER_OBTENER_PLANILLA_DETALLE = "CURSDET";

	/* PARAMETROS DE STORES PROCEDURES */
	public static final String P_LOTE = "P_LOTE";
	public static final String P_PLANILLA_INTERNO = "P_PLANILLA_INTERNO";
	public static final String P_PAGINA = "P_PAGINA";
	public static final String P_SECUENCIA = "P_SECUENCIA";
	public static final String P_CUSPP = "P_CUSPP";
	public static final String P_NRO_PREIMPRESO = "P_NRO_PREIMPRESO";
	public static final String P_TIPO_PLANILLA = "P_TIPO_PLANILLA";
	public static final String P_ESTADO_PLANILLA = "P_ESTADO_PLANILLA";
	public static final String P_ESTADO_CONCILIACION = "P_ESTADO_CONCILIACION";
	public static final String P_PERIODO_DEVENGUE = "P_PERIODO_DEVENGUE";
	public static final String P_TIPO_IDENTIFICACION = "P_TIPO_IDENTIFICACION";
	public static final String P_NUMERO_IDENTIFICACION = "P_NUMERO_IDENTIFICACION";
	public static final String P_FECHA_PAGO_INICIAL = "P_FECHA_PAGO_INICIAL";
	public static final String P_FECHA_PAGO_FINAL = "P_FECHA_PAGO_FINAL";
	public static final String P_REGINI = "P_REGINI";
	public static final String P_REGFIN = "P_REGFIN";
	public static final String P_FECHAINI = "P_FECHAINI";
	public static final String P_FECHAFIN = "P_FECHAFIN";
	public static final String P_TIPODOCUMENTO_EMPLEADOR = "P_TIPODOCUMENTO_EMPLEADOR";
	public static final String P_NUMERODOCUMENTO_EMPLEADOR = "P_NUMERODOCUMENTO_EMPLEADOR";
	public static final String P_RAZON_SOCIAL = "P_RAZON_SOCIAL";
	public static final String P_CANAL = "P_CANAL";
	public static final String P_TIPODOCUMENTO_AFILIADO = "P_TIPODOCUMENTO_AFILIADO";
	public static final String P_NUMERODOCUMENTO_AFILIADO = "P_NUMERODOCUMENTO_AFILIADO";
	public static final String P_APELLIDO_PATERNO = "P_APELLIDO_PATERNO";
	public static final String P_APELLIDO_MATERNO = "P_APELLIDO_MATERNO";

	public static final String P_FECHA = "P_FECHA";
	public static final String P_COD_ESTADO_ASOC_AFP = "P_COD_ESTADO_ASOC_AFP";
	public static final String P_COD_ESTADO_SBS = "P_COD_ESTADO_SBS";
	public static final String P_REGISTRO_INICIO = "P_REGISTRO_INICIO";
	public static final String P_REGISTRO_FIN = "P_REGISTRO_FIN";
	public static final String P_CANT = "P_CANT";

	public static final String HEADER_USER ="usuario";
	
	/* VALORES NUMERICOS MAPPERS */
	public static final Integer VALUE_ZERO = 0;
	public static final Integer VALUE_ONE = 1;

	/* MENSAJES Y CODIGOS DE RESPUESTA DE LA API */
	public static final int CODE_RESPONSE_SUCCESS = 10000;
	public static final String MSG_RESPONSE_SUCCESS = "Proceso ejecutado con éxito.";

	public static final int CODE_RESPONSE_NOT_DATA = 10001;
	public static final String MSG_RESPONSE_NOT_DATA = "No se encontraron resultados.";

	public static final int CODE_RESPONSE_ERROR = 10002;
	public static final String MSG_RESPONSE_ERROR = "Error interno. Por favor comuniquese con el administrador.";

	public static final int CODE_HEADER_USER_EMPTY = 10004;
	public static final String MSG_HEADER_USER_EMPTY = "Error de campos obligatorios. Por favor revise el campo 'usuario' como parte del Header";

	public static final int CODE_RESPONSE_FIELDS_VALIDATION = 10005;
	public static final String MESSAGE_FIELDS_VALIDATION = "Existen campos con formato incorrecto";

	public static final String MESSAGE_SUCCESSFUL = "Proceso ejecutado con éxito";
	public static final String MESSAGE_ERROR = "Proceso ejecutado con error";
	public static final String MESSAGE_DENIED = "Permisos denegados. Por favor revise el campo 'usuario' como parte del Header.";

}
