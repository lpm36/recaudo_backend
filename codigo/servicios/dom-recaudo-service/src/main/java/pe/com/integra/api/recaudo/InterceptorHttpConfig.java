package pe.com.integra.api.recaudo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import pe.com.integra.api.recaudo.infraestructure.security.SecurityInterceptor;

/**
 * Clase que intercepta todas las peticiones HTTP y habilita una capa de
 * seguridad para los recursos/endpoint expuestos.
 * 
 * Se excluye del interceptor la URL de documentación Swagger.
 *
 * @author MDP Consulting
 * @since 1.0
 */
@Configuration
public class InterceptorHttpConfig implements WebMvcConfigurer {

	@Bean
	public SecurityInterceptor securityInterceptor() {
		return new SecurityInterceptor();
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(securityInterceptor())
				.addPathPatterns("/api/**")
				.excludePathPatterns("/swagger*/**");
	}

}