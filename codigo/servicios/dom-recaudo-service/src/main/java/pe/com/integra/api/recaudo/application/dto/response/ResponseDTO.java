package pe.com.integra.api.recaudo.application.dto.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "ResponseDTO", description = "respuesta generica de la api")
public class ResponseDTO<A> {
	
	@ApiModelProperty(value = "estado", required = false, position = 1)
	private boolean status;
	
	@ApiModelProperty(value = "codigo interno de respuesta", required = false, position = 2)
	private int code;

	@ApiModelProperty(value = "mensaje de respuesta", required = false, position = 3)
	private String message;

	@ApiModelProperty(value = "Cuerpo de la respuesta", required = false, position = 4)
	private A body;
}
