package pe.com.integra.api.recaudo.application.dto;

import static org.mockito.Mockito.mock;

import org.junit.Test;
import org.mockito.Answers;

import pe.com.integra.api.recaudo.application.dto.response.ResponseObtenerCabeceraPlanillasDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseBusquedaPlanillasDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseBusquedaPlanillasDetalleDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponseDTO;
import pe.com.integra.api.recaudo.application.dto.response.ResponsePlanillaDetalleDTO;
import pe.com.integra.api.recaudo.domain.dao.model.mapping.ObtenerPlanillaCabeceraMapping;
import pe.com.integra.api.recaudo.domain.dao.model.mapping.BusquedaPlanillaDetalleMapping;
import pe.com.integra.api.recaudo.domain.dao.model.mapping.BusquedaPlanillaCabeceraMapping;

public class DtoTest {
	
	@Test
	public void probarGetterSetter() {
		ResponseObtenerCabeceraPlanillasDTO responseBusquedaCabeceraPlanillasDTO = mock(ResponseObtenerCabeceraPlanillasDTO.class, Answers.RETURNS_MOCKS);
		ResponseBusquedaPlanillasDetalleDTO responseBusquedaPlanillasDetalleDTO = mock(ResponseBusquedaPlanillasDetalleDTO.class, Answers.RETURNS_MOCKS);
		ResponseBusquedaPlanillasDTO responseBusquedaPlanillasDTO = mock(ResponseBusquedaPlanillasDTO.class, Answers.RETURNS_MOCKS);
		ResponsePlanillaDetalleDTO responsePlanillaDetalleDTO = mock(ResponsePlanillaDetalleDTO.class, Answers.RETURNS_MOCKS);
		ResponseDTO responseDTO  = mock(ResponseDTO.class, Answers.RETURNS_MOCKS);
	}
}
