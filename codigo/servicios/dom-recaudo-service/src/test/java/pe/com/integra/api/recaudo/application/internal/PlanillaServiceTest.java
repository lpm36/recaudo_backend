package pe.com.integra.api.recaudo.application.internal;

import static org.mockito.Mockito.mock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDTO;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.domain.dao.impl.PlanillaDaoImpl;
import pe.com.integra.api.recaudo.domain.dao.model.Planilla;
import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;
import pe.com.integra.api.recaudo.infraestructure.exception.NotDataException;
import pe.com.integra.api.recaudo.infraestructure.util.Constantes;

@RunWith(SpringRunner.class)
public class PlanillaServiceTest {
	@Rule
	public final ExpectedException exception = ExpectedException.none();

	@Mock
	private PlanillaDaoImpl planillaDaoDummy;

	@InjectMocks
	private PlanillaServiceImpl planillaServiceImpl;

	@Test
	public void buscarPlanillasEntoncesExito() {
		RequestBusquedaPlanillaDTO request = new RequestBusquedaPlanillaDTO();

		Map<String, Object> resultadoDummy = new HashMap<String, Object>();
		List<Planilla> listaPlanillaDummy = new ArrayList<Planilla>();

		resultadoDummy.put(Constantes.MAPPER_PLANILLA_BUS, listaPlanillaDummy);

		Mockito.when(planillaDaoDummy.buscarPlanillas(request)).thenReturn(resultadoDummy);

		planillaServiceImpl.buscarPlanillas(request);

		Assert.assertTrue(true);
	}

	@Test
	public void buscarPlanillasEntoncesDevuelveUnaPlanilla() {
		RequestBusquedaPlanillaDTO request = mock(RequestBusquedaPlanillaDTO.class, Answers.RETURNS_MOCKS);
		Map<String, Object> resultadoDummy = new HashMap<String, Object>();
		List<Planilla> listaPlanillaDummy = new ArrayList<Planilla>();

		Planilla planillaDummy = mock(Planilla.class, Answers.RETURNS_MOCKS);
		listaPlanillaDummy.add(planillaDummy);

		resultadoDummy.put(Constantes.MAPPER_PLANILLA_BUS, listaPlanillaDummy);

		Mockito.when(planillaDaoDummy.buscarPlanillas(request)).thenReturn(resultadoDummy);

		planillaServiceImpl.buscarPlanillas(request);

		Assert.assertTrue(true);
	}

	@Test
	public void obtenerCabeceraPlanillaExito() {
		String usuario = "DummyUser";
		BigDecimal numeroPlanilla = BigDecimal.valueOf(100);
		BigDecimal lotePlanilla = BigDecimal.valueOf(100);
		List<Planilla> listaPlanillaDummy = new ArrayList<Planilla>();
		Planilla planillaDummy = new Planilla();

		listaPlanillaDummy.add(planillaDummy);

		Mockito.when(planillaDaoDummy.obtenerPlanilla(lotePlanilla, numeroPlanilla)).thenReturn(listaPlanillaDummy);

		planillaServiceImpl.obtenerCabeceraPlanilla(usuario, lotePlanilla, numeroPlanilla);

		Assert.assertTrue(true);
	}

	@Test
	public void obtenerCabeceraPlanillaCuandoNoseEncuentranPlanillasEntoncesArrojaRecursoNoEncontradoException() {
		String usuario = "DummyUser";
		BigDecimal numeroPlanilla = BigDecimal.valueOf(100);
		BigDecimal lotePlanilla = BigDecimal.valueOf(100);
		List<Planilla> listaPlanillaDummy = new ArrayList<Planilla>();

		Mockito.when(planillaDaoDummy.obtenerPlanilla(lotePlanilla, numeroPlanilla)).thenReturn(listaPlanillaDummy);

		exception.expect(NotDataException.class);

		planillaServiceImpl.obtenerCabeceraPlanilla(usuario, lotePlanilla, numeroPlanilla);

		Assert.assertTrue(true);
	}

	@Test
	public void buscarPlanillasDetallesExito() {
		RequestBusquedaPlanillaDetalleDTO request = new RequestBusquedaPlanillaDetalleDTO();
		List<PlanillaDetalle> listaPlanillaDetalleDummy = new ArrayList<PlanillaDetalle>();
		Map<String, Object> resultadoDummy = new HashMap<String, Object>();

		resultadoDummy.put(Constantes.MAPPER_PLANILLA_DET, listaPlanillaDetalleDummy);

		Mockito.when(planillaDaoDummy.buscarPlanillasDetalles(Mockito.any())).thenReturn(resultadoDummy);

		planillaServiceImpl.buscarPlanillasDetalles(request);

		Assert.assertTrue(true);
	}

	@Test
	public void buscarPlanillasDetallesEntoncesDevuelveUnDetalle() {
		RequestBusquedaPlanillaDetalleDTO request = mock(RequestBusquedaPlanillaDetalleDTO.class,
				Answers.RETURNS_MOCKS);
		List<PlanillaDetalle> listaPlanillaDetalleDummy = new ArrayList<PlanillaDetalle>();
		Map<String, Object> resultadoDummy = new HashMap<String, Object>();
		PlanillaDetalle planillaDetalleDummy = mock(PlanillaDetalle.class, Answers.RETURNS_MOCKS);

		listaPlanillaDetalleDummy.add(planillaDetalleDummy);
		resultadoDummy.put(Constantes.MAPPER_PLANILLA_DET, listaPlanillaDetalleDummy);

		Mockito.when(planillaDaoDummy.buscarPlanillasDetalles(Mockito.any())).thenReturn(resultadoDummy);

		planillaServiceImpl.buscarPlanillasDetalles(request);

		Assert.assertTrue(true);
	}

	@Test
	public void obtenerDetallePlanillaExito() {
		BigDecimal numeroPlanilla = BigDecimal.valueOf(100);
		BigDecimal lotePlanilla = BigDecimal.valueOf(100);
		BigDecimal pagina = BigDecimal.valueOf(1);
		BigDecimal secuencia = BigDecimal.valueOf(2);

		PlanillaDetalle parametrosDetalle = new PlanillaDetalle();
		parametrosDetalle.setLote(lotePlanilla);
		parametrosDetalle.setPlanillaInterno(numeroPlanilla);
		parametrosDetalle.setPagina(pagina);
		parametrosDetalle.setSecuencia(secuencia);

		List<PlanillaDetalle> listaPlanillaDetalleDummy = new ArrayList<PlanillaDetalle>();
		PlanillaDetalle planillaDetalleDummy = new PlanillaDetalle();
		listaPlanillaDetalleDummy.add(planillaDetalleDummy);

		List<Planilla> listaPlanillaDummy = new ArrayList<Planilla>();
		Planilla planillaDummy = new Planilla();
		listaPlanillaDummy.add(planillaDummy);

		Map<String, Object> resultado = new HashMap<String, Object>();
		resultado.put(Constantes.MAPPER_OBTENER_PLANILLA_DETALLE, listaPlanillaDetalleDummy);
		resultado.put(Constantes.MAPPER_PLANILLA_BUS, listaPlanillaDummy);

		Mockito.when(planillaDaoDummy.obtenerDetallePlanilla(Mockito.any())).thenReturn(resultado);

		planillaServiceImpl.obtenerDetallePlanilla(parametrosDetalle);

		Assert.assertTrue(true);
	}

	@Test
	public void obtenerDetallePlanillaCuandoNoseEncuentranPlanillasEntoncesArrojaRecursoNoEncontradoException() {
		BigDecimal numeroPlanilla = BigDecimal.valueOf(100);
		BigDecimal lotePlanilla = BigDecimal.valueOf(100);
		BigDecimal pagina = BigDecimal.valueOf(1);
		BigDecimal secuencia = BigDecimal.valueOf(2);

		PlanillaDetalle parametrosDetalle = new PlanillaDetalle();
		parametrosDetalle.setLote(lotePlanilla);
		parametrosDetalle.setPlanillaInterno(numeroPlanilla);
		parametrosDetalle.setPagina(pagina);
		parametrosDetalle.setSecuencia(secuencia);

		List<PlanillaDetalle> listaPlanillaDetalleDummy = new ArrayList<PlanillaDetalle>();
		List<Planilla> listaPlanillaDummy = new ArrayList<Planilla>();

		Map<String, Object> resultado = new HashMap<String, Object>();

		resultado.put(Constantes.MAPPER_OBTENER_PLANILLA_DETALLE, listaPlanillaDetalleDummy);
		resultado.put(Constantes.MAPPER_PLANILLA_BUS, listaPlanillaDummy);

		Mockito.when(planillaDaoDummy.obtenerDetallePlanilla(Mockito.any())).thenReturn(resultado);

		exception.expect(NotDataException.class);

		planillaServiceImpl.obtenerDetallePlanilla(parametrosDetalle);

		Assert.assertTrue(true);
	}

	@Test
	public void obtenerDetallePlanillaCuandoNoseEncuentranDetallesEntoncesArrojaRecursoNoEncontradoException() {
		BigDecimal numeroPlanilla = BigDecimal.valueOf(100);
		BigDecimal lotePlanilla = BigDecimal.valueOf(100);
		BigDecimal pagina = BigDecimal.valueOf(1);
		BigDecimal secuencia = BigDecimal.valueOf(2);

		PlanillaDetalle parametrosDetalle = new PlanillaDetalle();
		parametrosDetalle.setLote(lotePlanilla);
		parametrosDetalle.setPlanillaInterno(numeroPlanilla);
		parametrosDetalle.setPagina(pagina);
		parametrosDetalle.setSecuencia(secuencia);

		List<PlanillaDetalle> listaPlanillaDetalleDummy = new ArrayList<PlanillaDetalle>();
		List<Planilla> listaPlanillaDummy = new ArrayList<Planilla>();
		Planilla planillaDummy = new Planilla();
		listaPlanillaDummy.add(planillaDummy);

		Map<String, Object> resultado = new HashMap<String, Object>();

		resultado.put(Constantes.MAPPER_OBTENER_PLANILLA_DETALLE, listaPlanillaDetalleDummy);
		resultado.put(Constantes.MAPPER_PLANILLA_BUS, listaPlanillaDummy);

		Mockito.when(planillaDaoDummy.obtenerDetallePlanilla(Mockito.any())).thenReturn(resultado);

		exception.expect(NotDataException.class);

		planillaServiceImpl.obtenerDetallePlanilla(parametrosDetalle);

		Assert.assertTrue(true);
	}
}