package pe.com.integra.api.recaudo.domain.dao.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.test.context.junit4.SpringRunner;

import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDTO;
import pe.com.integra.api.recaudo.application.dto.request.RequestBusquedaPlanillaDetalleDTO;
import pe.com.integra.api.recaudo.domain.dao.model.Planilla;
import pe.com.integra.api.recaudo.domain.dao.model.PlanillaDetalle;
import pe.com.integra.api.recaudo.infraestructure.util.Constantes;
import pe.com.integra.api.recaudo.infraestructure.util.UtilDatabase;

@RunWith(SpringRunner.class)
public class PlanillaDaoImplTest {

	@InjectMocks
	private PlanillaDaoImpl planillaDao;

	@Mock
	private UtilDatabase utilDatabaseDummy;

	@Test
	public void buscarPlanillasExito() {

		RequestBusquedaPlanillaDTO request = new RequestBusquedaPlanillaDTO();

		Map<String, Object> resultExecute = new HashMap<String, Object>();

		cambiaJdbcExecute(resultExecute);
		
		Map<String, Object> result = planillaDao.buscarPlanillas(request);
		
		Assert.assertTrue(true);
	}

	@Test
	public void obtenerPlanillaExito() {
		BigDecimal lotePlanilla = new BigDecimal(0);
		BigDecimal numeroPlanilla = new BigDecimal(0);

		List<Planilla> listaPlanillasDummy = new ArrayList<Planilla>();
		Planilla planillaDummy = new Planilla();
		Planilla planillaDummy2 = new Planilla();
		listaPlanillasDummy.add(planillaDummy);
		listaPlanillasDummy.add(planillaDummy2);

		Map<String, Object> resultExecute = new HashMap<String, Object>();
		resultExecute.put(Constantes.MAPPER_PLANILLA_CAB, listaPlanillasDummy);

		cambiaJdbcExecute(resultExecute);
		
		List<Planilla> listaPlanilla = planillaDao.obtenerPlanilla(lotePlanilla, numeroPlanilla);

		Assert.assertTrue(true);
	}

	@Test
	public void buscarPlanillasDetallesExito() {
		RequestBusquedaPlanillaDetalleDTO request = new RequestBusquedaPlanillaDetalleDTO();

		Map<String, Object> resultExecute = new HashMap<String, Object>();

		cambiaJdbcExecute(resultExecute);
		
		Map<String, Object> result = planillaDao.buscarPlanillasDetalles(request);
		
		Assert.assertTrue(true);
	}

	@Test
	public void obtenerDetallePlanillaExito() {
		PlanillaDetalle request = new PlanillaDetalle();

		Map<String, Object> resultExecute = new HashMap<String, Object>();

		cambiaJdbcExecute(resultExecute);
		
		Map<String, Object> result = planillaDao.obtenerDetallePlanilla(request);
		
		Assert.assertTrue(true);
	}

	private void cambiaJdbcExecute(Map<String, Object> resultExecute) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		SimpleJdbcCallStub simpleJdbcCallStub = new SimpleJdbcCallStub(jdbcTemplate, resultExecute);
		Mockito.when(utilDatabaseDummy.getSimpleDb2(Mockito.any(), Mockito.any())).thenReturn(simpleJdbcCallStub);
	}

	class SimpleJdbcCallStub extends SimpleJdbcCall {

		private Map<String, Object> resultExecute;

		public SimpleJdbcCallStub(JdbcTemplate jdbcTemplate, Map<String, Object> resultExecute) {
			super(jdbcTemplate);
			this.resultExecute = resultExecute;
		}

		@Override
		public Map<String, Object> execute(SqlParameterSource parameterSource) {
			return resultExecute;
		}
	}
}