package pe.com.integra.api.recaudo.view.rest;

import static org.hamcrest.core.Is.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import net.minidev.json.JSONObject;
import pe.com.integra.api.recaudo.application.PlanillaService;
import pe.com.integra.api.recaudo.infraestructure.exception.NotDataException;
import pe.com.integra.api.recaudo.infraestructure.util.Constantes;

@RunWith(SpringRunner.class)
@WebMvcTest(PlanillaController.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PlanillaControllerTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private PlanillaService planillaService;

	private static final String URL_OBTENER_PLANILLA_CABECERA = "/api/planilla/{lote_planilla}-{numero_planilla}";
	private static final String URL_BUSQUEDA_PLANILLA_DETALLE = "/api/planilla/busquedadetalle";
	private static final String URL_OBTENER_PLANILLA_DETALLE = "/api/planilla/{lote_planilla}-{numero_planilla}/detalle/{pagina}-{secuencia}";
	private static final String URL_BUSQUEDA_PLANILLA = "/api/planilla/busqueda";

	@Before
	public void setup() throws Exception {

	}

	@Test
	public void postBuscarPlanillaExito() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isOk()).andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaCuandoEnviaFormatosDeFechaCorrectaEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("periodoDevengue", "201901");
		json.put("fechaPagoInicial", "20190101");
		json.put("fechaPagoFinal", "20190102");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isOk()).andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaCuandoEnviaFormatosDeFechaIncorrectaEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("periodoDevengue", "201913");
		json.put("fechaPagoInicial", "20190101");
		json.put("fechaPagoFinal", "20190102");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaCuandoEnviaFechasVaciasEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("periodoDevengue", "");
		json.put("fechaPagoInicial", "");
		json.put("fechaPagoFinal", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isOk()).andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaCuandoNoHayCriteriosDePrimerNivelEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content("{\r\n" + "  \"registroInicial\": 1,\r\n" + "  \"registroFinal\": 10\r\n" + "}"))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoHayCriterioDePrimerNivelPlanillaInternoEntoncesDevuelveOk()
			throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("planillaInterno", 1);
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaCuandoLoteNumeroExito() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("periodoDevengue", "");
		json.put("fechaPagoInicial", "");
		json.put("fechaPagoFinal", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isOk()).andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)));
	}

	@Test
	public void postBuscarPlanillaCuandoLoteEsCadenaDevuelveInternalServerError() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", "abc");
		json.put("periodoDevengue", "");
		json.put("fechaPagoInicial", "");
		json.put("fechaPagoFinal", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isInternalServerError())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)));
	}

	@Test
	public void postBuscarPlanillaCuandoPlanillaInternoEsCadenaDevuelveInternalServerError() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("planillaInterno", "abc");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isInternalServerError())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)));
	}

	@Test
	public void postBuscarPlanillaCuandoRegistroInicialEsCadenaDevuelveInternalServerError() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("registroInicial", "a");
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isInternalServerError())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)));
	}

	@Test
	public void postBuscarPlanillaCuandoRegistroFinalEsCadenaDevuelveInternalServerError() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("registroInicial", 1);
		json.put("registroFinal", "b");

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isInternalServerError())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoHayTipoYNoNumeroEntoncesDevuelveBadRequest() throws Exception {
		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("tipoIdentificacion", "RUC");
		json.put("numeroIdentificacion", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isBadRequest())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaDetalleLoteEsCadenaEntoncesDevuelveInternalServerError() throws Exception {
		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", "abc");
		json.put("registroInicial", 1);
		json.put("registroFinal", 2);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString()))
				.andExpect(status().isInternalServerError()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)));
	}

	@Test
	public void postBuscarPlanillaDetallePlanillaInternoEsCadenaEntoncesDevuelveInternalServerError() throws Exception {
		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("planillaInterno", "abc");
		json.put("registroInicial", 1);
		json.put("registroFinal", 2);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString()))
				.andExpect(status().isInternalServerError()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)));
	}

	@Test
	public void postBuscarPlanillaDetalleRegistroInicialEsCadenaEntoncesDevuelveInternalServerError() throws Exception {
		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("registroInicial", "a");
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString()))
				.andExpect(status().isInternalServerError()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)));
	}

	@Test
	public void postBuscarPlanillaDetalleRegistroFinalEsCadenaEntoncesDevuelveInternalServerError() throws Exception {
		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("registroInicial", 1);
		json.put("registroFinal", "b");

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString()))
				.andExpect(status().isInternalServerError()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoNoHayTipoYHayNumeroEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("tipoIdentificacion", "");
		json.put("numeroIdentificacion", "020202");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isBadRequest())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoHayTipoYNullNumeroEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("tipoIdentificacion", "RUC");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(
				post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
						.content("{\r\n" + "  \"tipoIdentificacion\": \"RUC\",\r\n" + "  \"registroInicial\": 1,\r\n"
								+ "  \"registroFinal\": 10\r\n" + "}"))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoNullTipoYHayNumeroEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("numeroIdentificacion", "2020");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isBadRequest())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoNoHayPeriodoEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("periodoDevengue", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(
				post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
						.content("{\r\n" + "  \"periodoDevengue\": \"\",\r\n" + "  \"registroInicial\": 1,\r\n"
								+ "  \"registroFinal\": 10\r\n" + "}"))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoNoHayCusppEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("cuspp", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isBadRequest())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoHayPeriodoEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("periodoDevengue", "201812");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoHayCusppEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("cuspp", "101051510");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content((json.toString()))).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaDetalleCuandoHayTipoYNumeroEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("tipoIdentificacion", "RUC");
		json.put("numeroIdentificacion", "2020");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content((json.toString()))).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaCuandoNoHayPeriodoEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("periodoDevengue", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaCuandoHayPeriodoEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("periodoDevengue", "201812");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isOk()).andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaCuandoNoHayNroPreimpresoEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("nroPreimpreso", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content("{\r\n" + "  \"nroPreimpreso\": \"\",\r\n" + "  \"registroInicial\": 1,\r\n"
						+ "  \"registroFinal\": 10\r\n" + "}"))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaCuandoHayNroPreimpresoEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("nroPreimpreso", "1234");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content("{\r\n" + "  \"nroPreimpreso\": \"1234\",\r\n" + "  \"registroInicial\": 1,\r\n"
						+ "  \"registroFinal\": 10\r\n" + "}"))
				.andExpect(status().isOk()).andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaCuandoHayPlanillaInternoEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("planillaInterno", 1234);
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isOk()).andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void getObtenerCabeceraPlanillaExito() throws Exception {
		String numeroPlanilla = "1";
		String lotePlanilla = "1";

		Mockito.when(planillaService.obtenerCabeceraPlanilla(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenReturn(null);

		mockMvc.perform(get(URL_OBTENER_PLANILLA_CABECERA, numeroPlanilla, lotePlanilla).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));

	}

	@Test
	public void getObtenerCabeceraPlanillaCuandoNoDevuelveRegistrosEntoncesMuestraMensajeNoHayDatos() throws Exception {
		String numeroPlanilla = "1";
		String lotePlanilla = "1";
		String mensajeError = "mensaje";
		String body = (new ObjectMapper()).valueToTree(numeroPlanilla).toString();

		Mockito.when(planillaService.obtenerCabeceraPlanilla(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenThrow(new NotDataException(mensajeError));

		mockMvc.perform(get(URL_OBTENER_PLANILLA_CABECERA, numeroPlanilla, lotePlanilla).content(body)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_NOT_DATA)))
				.andExpect(jsonPath("message", is(Constantes.MSG_RESPONSE_NOT_DATA)))
				.andExpect(jsonPath("body", is(mensajeError)));
	}

	@Test
	public void getObtenerCabeceraPlanillaCuandoPlanillaEsNegativoEntoncesMuestraMensajeCamposInvalidos()
			throws Exception {
		String numeroPlanilla = "-1";
		String lotePlanilla = "1";
		String body = (new ObjectMapper()).valueToTree(numeroPlanilla).toString();

		mockMvc.perform(get(URL_OBTENER_PLANILLA_CABECERA, numeroPlanilla, lotePlanilla).content(body)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void getObtenerCabeceraPlanillaCuandoPlanillaEsStringEntoncesMuestraMensajeCamposInvalidos()
			throws Exception {
		String numeroPlanilla = "abc";
		String lotePlanilla = "12";
		String body = (new ObjectMapper()).valueToTree(numeroPlanilla).toString();

		mockMvc.perform(get(URL_OBTENER_PLANILLA_CABECERA, numeroPlanilla, lotePlanilla).content(body)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void getObtenerCabeceraPlanillaCuandoLoteEsStringEntoncesMuestraMensajeCamposInvalidos() throws Exception {
		String numeroPlanilla = "12";
		String lotePlanilla = "def";
		String body = (new ObjectMapper()).valueToTree(numeroPlanilla).toString();

		mockMvc.perform(get(URL_OBTENER_PLANILLA_CABECERA, numeroPlanilla, lotePlanilla).content(body)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void getObtenerCabeceraPlanillaCuandoNoEnviaUsuarioEntoncesRespondeBadRequest() throws Exception {

		String lotePlanilla = "1";
		String numeroPlanilla = "1";

		Mockito.when(planillaService.obtenerDetallePlanilla(Mockito.any())).thenReturn(null);

		mockMvc.perform(get(URL_OBTENER_PLANILLA_CABECERA, numeroPlanilla, lotePlanilla)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_HEADER_USER_EMPTY)))
				.andExpect(jsonPath("message", is(Constantes.MSG_HEADER_USER_EMPTY)));
	}

	@Test
	public void getObtenerCabeceraPlanillaCuandoDevuelveExcepcionGenericaEntoncesMuestraMensajeExcepcion()
			throws Exception {
		String numeroPlanilla = "1";
		String lotePlanilla = "1";

		Mockito.when(planillaService.obtenerCabeceraPlanilla(Mockito.any(), Mockito.any(), Mockito.any()))
				.thenThrow(new RuntimeException());

		mockMvc.perform(get(URL_OBTENER_PLANILLA_CABECERA, numeroPlanilla, lotePlanilla).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isInternalServerError())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_ERROR)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_ERROR)));
	}

	@Test
	public void postBuscarDetallesPlanillaExito() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarDetallePlanillaCuandoNoHayCriteriosDePrimerNivelEntoncesDevuelveBadRequest()
			throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isBadRequest())
				.andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaCuandoHayCriterioDePrimerNivelPlanillaInternoEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillasDetalles(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("planillaInterno", 1);
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA_DETALLE).header("usuario", "usuario")
				.contentType(MediaType.APPLICATION_JSON).content(json.toString())).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void postBuscarPlanillaCuandoHayTipoYNoNumeroEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("tipoIdentificacion", "RUC");
		json.put("numeroIdentificacion", "");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaCuandoNoHayTipoYHayNumeroEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("tipoIdentificacion", "");
		json.put("numeroIdentificacion", "020202");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaCuandoHayTipoYNullNumeroEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("tipoIdentificacion", "RUC");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaCuandoNullTipoYHayNumeroEntoncesDevuelveBadRequest() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("numeroIdentificacion", "2020");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void postBuscarPlanillaCuandoHayTipoYNumeroEntoncesDevuelveOk() throws Exception {

		Mockito.when(planillaService.buscarPlanillas(Mockito.any())).thenReturn(null);

		JSONObject json = new JSONObject();
		json.put("lote", 448);
		json.put("tipoIdentificacion", "RUC");
		json.put("numeroIdentificacion", "2020");
		json.put("registroInicial", 1);
		json.put("registroFinal", 10);

		mockMvc.perform(post(URL_BUSQUEDA_PLANILLA).header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)
				.content(json.toString())).andExpect(status().isOk()).andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void getObtenerDetallePlanillaExito() throws Exception {

		String lotePlanilla = "1";
		String numeroPlanilla = "1";
		String pagina = "1";
		String secuencia = "1";

		Mockito.when(planillaService.obtenerDetallePlanilla(Mockito.any())).thenReturn(null);

		mockMvc.perform(get(URL_OBTENER_PLANILLA_DETALLE, numeroPlanilla, lotePlanilla, pagina, secuencia)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(jsonPath("status", is(true)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_SUCCESS)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_SUCCESSFUL)));
	}

	@Test
	public void getObtenerCabeceraPlanillaDetalleCuandoPlanillaEsStringEntoncesMuestraMensajeCamposInvalidos()
			throws Exception {
		String numeroPlanilla = "abc";
		String lotePlanilla = "1";
		String pagina = "1";
		String secuencia = "1";

		mockMvc.perform(get(URL_OBTENER_PLANILLA_DETALLE, numeroPlanilla, lotePlanilla, pagina, secuencia)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void getObtenerCabeceraPlanillaDetalleCuandoLoteEsStringEntoncesMuestraMensajeCamposInvalidos()
			throws Exception {
		String numeroPlanilla = "1";
		String lotePlanilla = "abc";
		String pagina = "1";
		String secuencia = "1";

		mockMvc.perform(get(URL_OBTENER_PLANILLA_DETALLE, numeroPlanilla, lotePlanilla, pagina, secuencia)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void getObtenerCabeceraPlanillaDetalleCuandoPaginaEsStringEntoncesMuestraMensajeCamposInvalidos()
			throws Exception {
		String numeroPlanilla = "1";
		String lotePlanilla = "1";
		String pagina = "abc";
		String secuencia = "1";

		mockMvc.perform(get(URL_OBTENER_PLANILLA_DETALLE, numeroPlanilla, lotePlanilla, pagina, secuencia)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}

	@Test
	public void getObtenerCabeceraPlanillaDetalleCuandoSecuenciaEsStringEntoncesMuestraMensajeCamposInvalidos()
			throws Exception {
		String numeroPlanilla = "1";
		String lotePlanilla = "1";
		String pagina = "1";
		String secuencia = "abc";

		mockMvc.perform(get(URL_OBTENER_PLANILLA_DETALLE, numeroPlanilla, lotePlanilla, pagina, secuencia)
				.header("usuario", "usuario").contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isBadRequest()).andExpect(jsonPath("status", is(false)))
				.andExpect(jsonPath("code", is(Constantes.CODE_RESPONSE_FIELDS_VALIDATION)))
				.andExpect(jsonPath("message", is(Constantes.MESSAGE_FIELDS_VALIDATION)));
	}
}